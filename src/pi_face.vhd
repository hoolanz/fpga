library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity pi_face is
	port(
		rst : in std_logic;
		pi_ctl : in std_logic;
		pi_dat : in std_logic_vector(7 downto 0);
		valid  : out std_logic;
		output : out std_logic_vector(47 downto 0)
);
end pi_face;

architecture bhv of pi_face is
	signal s_output: std_logic_vector(47 downto 0);
	signal count: std_logic_vector(5 downto 0) := "100000";
begin

	output <= s_output;
	
	process(rst, pi_ctl)
	begin
		if(rst = '1') then
			s_output <= ( others => '0');
			count <= "100000";
		elsif(rising_edge(pi_ctl)) then
			s_output <= s_output(39 downto 0) & pi_dat;
			count <= count(0) & count(5 downto 1);
		end if;
	end process;
	
	valid <= count(5);
	
end bhv;