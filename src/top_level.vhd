-- top level entity

library ieee;
use ieee.std_logic_1164.all;

use work.rgbmatrix.all; -- Constants & Configuration

entity top_level is
    port (
		pi_rst		: in std_logic;
		pi_ctl		: in std_logic;
		pi_dat		: in std_logic_vector(7 downto 0);
	 
        clk_in  : in std_logic;
        rst_n   : in std_logic;
        clk_out : out std_logic;
        r1      : out std_logic;
        r2      : out std_logic;
        b1      : out std_logic;
        b2      : out std_logic;
        g1      : out std_logic;
        g2      : out std_logic;
        a       : out std_logic;
        b       : out std_logic;
        c       : out std_logic;
		  d       : out std_logic;
        lat     : out std_logic;
        oe      : out std_logic
    );
end top_level;

architecture str of top_level is
    -- Reset signals
    signal rst : std_logic;
    
    -- Memory signals
    signal addr : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal data_incoming : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal data_outgoing : std_logic_vector(DATA_WIDTH-1 downto 0);
    
    -- Flags
    signal data_valid : std_logic;
begin
    
    -- Reset button is an "active low" input, invert it so we can treat is as
    -- "active high", then OR it with the JTAG reset command output signal.
    rst <= not rst_n or pi_rst;
    
    -- LED panel controller
    U_LEDCTRL : entity work.ledctrl
        port map (
            rst => rst,
            clk_in => clk_in,
            -- Connection to LED panel
            clk_out => clk_out,
            rgb1(2) => r1,
            rgb1(1) => g1,
            rgb1(0) => b1,
            rgb2(2) => r2,
            rgb2(1) => g2,
            rgb2(0) => b2,
				led_addr(3) => d,
            led_addr(2) => c,
            led_addr(1) => b,
            led_addr(0) => a,
            lat => lat,
            oe  => oe,
            -- Connection with framebuffer
            addr => addr,
            data => data_outgoing
        );
		  
    U_PIFACE : entity work.pi_face
        port map (
            rst     => rst,
            output  => data_incoming,
            valid   => data_valid,
				pi_ctl  => pi_ctl,
				pi_dat  => pi_dat
        );
    
    -- Special memory for the framebuffer
    U_MEMORY : entity work.memory
        port map (
            rst => rst,
            -- Writing side
            clk_wr => data_valid,
            input  => data_incoming,
            -- Reading side
            clk_rd => clk_in,
            addr   => addr,
            output => data_outgoing
        );
    
end str;
