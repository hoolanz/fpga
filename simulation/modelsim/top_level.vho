-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "04/18/2014 20:24:33"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	top_level IS
    PORT (
	pi_rst : IN std_logic;
	pi_ctl : IN std_logic;
	pi_dat : IN std_logic_vector(7 DOWNTO 0);
	clk_in : IN std_logic;
	rst_n : IN std_logic;
	clk_out : OUT std_logic;
	r1 : OUT std_logic;
	r2 : OUT std_logic;
	b1 : OUT std_logic;
	b2 : OUT std_logic;
	g1 : OUT std_logic;
	g2 : OUT std_logic;
	a : OUT std_logic;
	b : OUT std_logic;
	c : OUT std_logic;
	d : OUT std_logic;
	lat : OUT std_logic;
	oe : OUT std_logic
	);
END top_level;

-- Design Ports Information
-- clk_out	=>  Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- r1	=>  Location: PIN_E7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- r2	=>  Location: PIN_F9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- b1	=>  Location: PIN_E8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- b2	=>  Location: PIN_C9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- g1	=>  Location: PIN_E10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- g2	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- a	=>  Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- b	=>  Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- c	=>  Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- d	=>  Location: PIN_E9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- lat	=>  Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oe	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- pi_rst	=>  Location: PIN_P11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- rst_n	=>  Location: PIN_J15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- clk_in	=>  Location: PIN_R8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_ctl	=>  Location: PIN_T10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[7]	=>  Location: PIN_T11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[6]	=>  Location: PIN_R12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[5]	=>  Location: PIN_T12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[4]	=>  Location: PIN_R13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[3]	=>  Location: PIN_T13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[2]	=>  Location: PIN_T14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[1]	=>  Location: PIN_T15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- pi_dat[0]	=>  Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF top_level IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_pi_rst : std_logic;
SIGNAL ww_pi_ctl : std_logic;
SIGNAL ww_pi_dat : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_clk_in : std_logic;
SIGNAL ww_rst_n : std_logic;
SIGNAL ww_clk_out : std_logic;
SIGNAL ww_r1 : std_logic;
SIGNAL ww_r2 : std_logic;
SIGNAL ww_b1 : std_logic;
SIGNAL ww_b2 : std_logic;
SIGNAL ww_g1 : std_logic;
SIGNAL ww_g2 : std_logic;
SIGNAL ww_a : std_logic;
SIGNAL ww_b : std_logic;
SIGNAL ww_c : std_logic;
SIGNAL ww_d : std_logic;
SIGNAL ww_lat : std_logic;
SIGNAL ww_oe : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \U_PIFACE|count[5]~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \rst~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk_in~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \pi_ctl~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk_out~output_o\ : std_logic;
SIGNAL \r1~output_o\ : std_logic;
SIGNAL \r2~output_o\ : std_logic;
SIGNAL \b1~output_o\ : std_logic;
SIGNAL \b2~output_o\ : std_logic;
SIGNAL \g1~output_o\ : std_logic;
SIGNAL \g2~output_o\ : std_logic;
SIGNAL \a~output_o\ : std_logic;
SIGNAL \b~output_o\ : std_logic;
SIGNAL \c~output_o\ : std_logic;
SIGNAL \d~output_o\ : std_logic;
SIGNAL \lat~output_o\ : std_logic;
SIGNAL \oe~output_o\ : std_logic;
SIGNAL \clk_in~input_o\ : std_logic;
SIGNAL \clk_in~inputclkctrl_outclk\ : std_logic;
SIGNAL \U_LEDCTRL|U_CLKDIV|count~0_combout\ : std_logic;
SIGNAL \pi_rst~input_o\ : std_logic;
SIGNAL \rst_n~input_o\ : std_logic;
SIGNAL \rst~combout\ : std_logic;
SIGNAL \rst~clkctrl_outclk\ : std_logic;
SIGNAL \U_LEDCTRL|U_CLKDIV|count~q\ : std_logic;
SIGNAL \U_LEDCTRL|U_CLKDIV|clk_out~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|U_CLKDIV|clk_out~q\ : std_logic;
SIGNAL \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[0]~7_combout\ : std_logic;
SIGNAL \U_LEDCTRL|state.LATCH~feeder_combout\ : std_logic;
SIGNAL \U_LEDCTRL|state.LATCH~q\ : std_logic;
SIGNAL \U_LEDCTRL|state.INIT~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|state.INIT~q\ : std_logic;
SIGNAL \U_LEDCTRL|next_state.READ_PIXEL_DATA~combout\ : std_logic;
SIGNAL \U_LEDCTRL|state.READ_PIXEL_DATA~q\ : std_logic;
SIGNAL \U_LEDCTRL|next_state.INCR_LED_ADDR~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|state.INCR_LED_ADDR~q\ : std_logic;
SIGNAL \U_LEDCTRL|Selector6~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[0]~8\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[1]~9_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Selector5~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[1]~10\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[2]~11_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Selector4~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[2]~12\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[3]~13_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Selector3~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[3]~14\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[4]~15_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Selector2~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[4]~16\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[5]~17_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Selector1~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[5]~18\ : std_logic;
SIGNAL \U_LEDCTRL|col_count[6]~19_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Selector0~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|next_state.INCR_RAM_ADDR~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|state.INCR_RAM_ADDR~q\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[0]~9_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Equal1~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Equal1~1_combout\ : std_logic;
SIGNAL \U_LEDCTRL|Equal1~2_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_led_addr[0]~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_led_addr[1]~1_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_led_addr[2]~2_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[7]~8_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_led_addr[3]~3_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[7]~25_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[0]~10\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[1]~11_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[1]~12\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[2]~13_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[2]~14\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[3]~15_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[3]~16\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[4]~17_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[4]~18\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[5]~19_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[5]~20\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[6]~21_combout\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[6]~22\ : std_logic;
SIGNAL \U_LEDCTRL|bpp_count[7]~23_combout\ : std_logic;
SIGNAL \pi_ctl~input_o\ : std_logic;
SIGNAL \pi_ctl~inputclkctrl_outclk\ : std_logic;
SIGNAL \U_PIFACE|count[4]~1_combout\ : std_logic;
SIGNAL \U_PIFACE|count[4]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|count[3]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|count[2]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|count[1]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|count[0]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|count[5]~0_combout\ : std_logic;
SIGNAL \U_PIFACE|count[5]~clkctrl_outclk\ : std_logic;
SIGNAL \pi_dat[7]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[7]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[15]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[23]~feeder_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[0]~27_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[1]~9_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[1]~10\ : std_logic;
SIGNAL \U_MEMORY|waddr[2]~11_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[2]~12\ : std_logic;
SIGNAL \U_MEMORY|waddr[3]~13_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[3]~14\ : std_logic;
SIGNAL \U_MEMORY|waddr[4]~15_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[4]~16\ : std_logic;
SIGNAL \U_MEMORY|waddr[5]~17_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[5]~18\ : std_logic;
SIGNAL \U_MEMORY|waddr[6]~19_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[6]~20\ : std_logic;
SIGNAL \U_MEMORY|waddr[7]~21_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[7]~22\ : std_logic;
SIGNAL \U_MEMORY|waddr[8]~23_combout\ : std_logic;
SIGNAL \U_MEMORY|waddr[8]~24\ : std_logic;
SIGNAL \U_MEMORY|waddr[9]~25_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[0]~9_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[1]~10_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[1]~11\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[2]~12_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[2]~13\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[3]~14_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[3]~15\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[4]~16_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[4]~17\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[5]~18_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[5]~19\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[6]~20_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[6]~21\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[7]~22_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[7]~23\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[8]~24_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[8]~25\ : std_logic;
SIGNAL \U_LEDCTRL|s_ram_addr[9]~26_combout\ : std_logic;
SIGNAL \pi_dat[0]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[0]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[8]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[16]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[24]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[32]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[40]~feeder_combout\ : std_logic;
SIGNAL \pi_dat[1]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[1]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[9]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[17]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[25]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[33]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[41]~feeder_combout\ : std_logic;
SIGNAL \pi_dat[2]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[2]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[10]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[18]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[26]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[34]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[42]~feeder_combout\ : std_logic;
SIGNAL \pi_dat[3]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[3]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[11]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[19]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[27]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[35]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[43]~feeder_combout\ : std_logic;
SIGNAL \pi_dat[4]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[12]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[20]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[28]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[36]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[44]~feeder_combout\ : std_logic;
SIGNAL \pi_dat[5]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[5]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[13]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[21]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[29]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[37]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[45]~feeder_combout\ : std_logic;
SIGNAL \pi_dat[6]~input_o\ : std_logic;
SIGNAL \U_PIFACE|s_output[6]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[14]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[22]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[30]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[38]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[46]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[31]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[39]~feeder_combout\ : std_logic;
SIGNAL \U_PIFACE|s_output[47]~feeder_combout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a47\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a46\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a45\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a44\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a43\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a42\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a41\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a40\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~1_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~3_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~5_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~7_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~9_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~11_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~13_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan0~14_combout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a22\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a21\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a20\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a19\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a18\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a17\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~1_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~3_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~5_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~7_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~9_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~11_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~13_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan3~14_combout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a31\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a30\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a29\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a28\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a27\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a26\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a25\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a24\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~1_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~3_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~5_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~7_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~9_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~11_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~13_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan2~14_combout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a7\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a6\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a4\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a3\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a2\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a1\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~1_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~3_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~5_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~7_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~9_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~11_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~13_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan5~14_combout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a39\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a38\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a37\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a36\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a35\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a34\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a33\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a32\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~1_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~3_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~5_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~7_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~9_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~11_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~13_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan1~14_combout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a15\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a14\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a13\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a12\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a10\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a9\ : std_logic;
SIGNAL \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~1_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~3_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~5_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~7_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~9_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~11_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~13_cout\ : std_logic;
SIGNAL \U_LEDCTRL|LessThan4~14_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_oe~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|s_rgb2\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \U_LEDCTRL|s_rgb1\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \U_LEDCTRL|s_ram_addr\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \U_LEDCTRL|s_led_addr\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \U_LEDCTRL|col_count\ : std_logic_vector(6 DOWNTO 0);
SIGNAL \U_LEDCTRL|bpp_count\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \U_PIFACE|s_output\ : std_logic_vector(47 DOWNTO 0);
SIGNAL \U_PIFACE|count\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \U_MEMORY|waddr\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \ALT_INV_rst~clkctrl_outclk\ : std_logic;
SIGNAL \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\ : std_logic;
SIGNAL \ALT_INV_rst~combout\ : std_logic;
SIGNAL \U_LEDCTRL|ALT_INV_s_oe~0_combout\ : std_logic;
SIGNAL \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\ : std_logic;
SIGNAL \U_LEDCTRL|ALT_INV_s_led_addr\ : std_logic_vector(3 DOWNTO 0);

BEGIN

ww_pi_rst <= pi_rst;
ww_pi_ctl <= pi_ctl;
ww_pi_dat <= pi_dat;
ww_clk_in <= clk_in;
ww_rst_n <= rst_n;
clk_out <= ww_clk_out;
r1 <= ww_r1;
r2 <= ww_r2;
b1 <= ww_b1;
b2 <= ww_b2;
g1 <= ww_g1;
g2 <= ww_g2;
a <= ww_a;
b <= ww_b;
c <= ww_c;
d <= ww_d;
lat <= ww_lat;
oe <= ww_oe;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTADATAIN_bus\ <= (\U_PIFACE|s_output\(47) & \U_PIFACE|s_output\(46) & \U_PIFACE|s_output\(45) & \U_PIFACE|s_output\(44) & \U_PIFACE|s_output\(43) & \U_PIFACE|s_output\(42) & 
\U_PIFACE|s_output\(41) & \U_PIFACE|s_output\(40) & \U_PIFACE|s_output\(23));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTAADDR_bus\ <= (\U_MEMORY|waddr\(9) & \U_MEMORY|waddr\(8) & \U_MEMORY|waddr\(7) & \U_MEMORY|waddr\(6) & \U_MEMORY|waddr\(5) & \U_MEMORY|waddr\(4) & \U_MEMORY|waddr\(3) & 
\U_MEMORY|waddr\(2) & \U_MEMORY|waddr\(1) & \U_MEMORY|waddr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBADDR_bus\ <= (\U_LEDCTRL|s_ram_addr\(9) & \U_LEDCTRL|s_ram_addr\(8) & \U_LEDCTRL|s_ram_addr\(7) & \U_LEDCTRL|s_ram_addr\(6) & \U_LEDCTRL|s_ram_addr\(5) & \U_LEDCTRL|s_ram_addr\(4) & 
\U_LEDCTRL|s_ram_addr\(3) & \U_LEDCTRL|s_ram_addr\(2) & \U_LEDCTRL|s_ram_addr\(1) & \U_LEDCTRL|s_ram_addr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(0);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a40\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(1);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a41\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(2);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a42\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(3);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a43\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(4);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a44\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(5);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a45\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(6);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a46\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(7);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a47\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\(8);

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTADATAIN_bus\ <= (\U_PIFACE|s_output\(31) & \U_PIFACE|s_output\(30) & \U_PIFACE|s_output\(22) & \U_PIFACE|s_output\(21) & \U_PIFACE|s_output\(20) & \U_PIFACE|s_output\(19) & 
\U_PIFACE|s_output\(18) & \U_PIFACE|s_output\(17) & \U_PIFACE|s_output\(16));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTAADDR_bus\ <= (\U_MEMORY|waddr\(9) & \U_MEMORY|waddr\(8) & \U_MEMORY|waddr\(7) & \U_MEMORY|waddr\(6) & \U_MEMORY|waddr\(5) & \U_MEMORY|waddr\(4) & \U_MEMORY|waddr\(3) & 
\U_MEMORY|waddr\(2) & \U_MEMORY|waddr\(1) & \U_MEMORY|waddr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBADDR_bus\ <= (\U_LEDCTRL|s_ram_addr\(9) & \U_LEDCTRL|s_ram_addr\(8) & \U_LEDCTRL|s_ram_addr\(7) & \U_LEDCTRL|s_ram_addr\(6) & \U_LEDCTRL|s_ram_addr\(5) & \U_LEDCTRL|s_ram_addr\(4) & 
\U_LEDCTRL|s_ram_addr\(3) & \U_LEDCTRL|s_ram_addr\(2) & \U_LEDCTRL|s_ram_addr\(1) & \U_LEDCTRL|s_ram_addr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(0);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a17\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(1);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a18\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(2);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a19\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(3);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a20\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(4);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a21\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(5);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a22\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(6);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a30\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(7);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a31\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\(8);

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTADATAIN_bus\ <= (\U_PIFACE|s_output\(29) & \U_PIFACE|s_output\(28) & \U_PIFACE|s_output\(27) & \U_PIFACE|s_output\(26) & \U_PIFACE|s_output\(25) & \U_PIFACE|s_output\(24) & 
\U_PIFACE|s_output\(7) & \U_PIFACE|s_output\(6) & \U_PIFACE|s_output\(5));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTAADDR_bus\ <= (\U_MEMORY|waddr\(9) & \U_MEMORY|waddr\(8) & \U_MEMORY|waddr\(7) & \U_MEMORY|waddr\(6) & \U_MEMORY|waddr\(5) & \U_MEMORY|waddr\(4) & \U_MEMORY|waddr\(3) & 
\U_MEMORY|waddr\(2) & \U_MEMORY|waddr\(1) & \U_MEMORY|waddr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBADDR_bus\ <= (\U_LEDCTRL|s_ram_addr\(9) & \U_LEDCTRL|s_ram_addr\(8) & \U_LEDCTRL|s_ram_addr\(7) & \U_LEDCTRL|s_ram_addr\(6) & \U_LEDCTRL|s_ram_addr\(5) & \U_LEDCTRL|s_ram_addr\(4) & 
\U_LEDCTRL|s_ram_addr\(3) & \U_LEDCTRL|s_ram_addr\(2) & \U_LEDCTRL|s_ram_addr\(1) & \U_LEDCTRL|s_ram_addr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(0);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a6\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(1);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a7\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(2);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a24\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(3);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a25\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(4);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a26\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(5);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a27\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(6);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a28\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(7);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a29\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\(8);

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\ <= (\U_PIFACE|s_output\(39) & \U_PIFACE|s_output\(38) & \U_PIFACE|s_output\(37) & \U_PIFACE|s_output\(36) & \U_PIFACE|s_output\(4) & \U_PIFACE|s_output\(3) & 
\U_PIFACE|s_output\(2) & \U_PIFACE|s_output\(1) & \U_PIFACE|s_output\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\ <= (\U_MEMORY|waddr\(9) & \U_MEMORY|waddr\(8) & \U_MEMORY|waddr\(7) & \U_MEMORY|waddr\(6) & \U_MEMORY|waddr\(5) & \U_MEMORY|waddr\(4) & \U_MEMORY|waddr\(3) & 
\U_MEMORY|waddr\(2) & \U_MEMORY|waddr\(1) & \U_MEMORY|waddr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\ <= (\U_LEDCTRL|s_ram_addr\(9) & \U_LEDCTRL|s_ram_addr\(8) & \U_LEDCTRL|s_ram_addr\(7) & \U_LEDCTRL|s_ram_addr\(6) & \U_LEDCTRL|s_ram_addr\(5) & \U_LEDCTRL|s_ram_addr\(4) & 
\U_LEDCTRL|s_ram_addr\(3) & \U_LEDCTRL|s_ram_addr\(2) & \U_LEDCTRL|s_ram_addr\(1) & \U_LEDCTRL|s_ram_addr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(0);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a1\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(1);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a2\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(2);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a3\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(3);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a4\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(4);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a36\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(5);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a37\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(6);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a38\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(7);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a39\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\(8);

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTADATAIN_bus\ <= (\U_PIFACE|s_output\(35) & \U_PIFACE|s_output\(34) & \U_PIFACE|s_output\(33) & \U_PIFACE|s_output\(32) & \U_PIFACE|s_output\(15) & \U_PIFACE|s_output\(14) & 
\U_PIFACE|s_output\(13) & \U_PIFACE|s_output\(12) & \U_PIFACE|s_output\(11));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTAADDR_bus\ <= (\U_MEMORY|waddr\(9) & \U_MEMORY|waddr\(8) & \U_MEMORY|waddr\(7) & \U_MEMORY|waddr\(6) & \U_MEMORY|waddr\(5) & \U_MEMORY|waddr\(4) & \U_MEMORY|waddr\(3) & 
\U_MEMORY|waddr\(2) & \U_MEMORY|waddr\(1) & \U_MEMORY|waddr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBADDR_bus\ <= (\U_LEDCTRL|s_ram_addr\(9) & \U_LEDCTRL|s_ram_addr\(8) & \U_LEDCTRL|s_ram_addr\(7) & \U_LEDCTRL|s_ram_addr\(6) & \U_LEDCTRL|s_ram_addr\(5) & \U_LEDCTRL|s_ram_addr\(4) & 
\U_LEDCTRL|s_ram_addr\(3) & \U_LEDCTRL|s_ram_addr\(2) & \U_LEDCTRL|s_ram_addr\(1) & \U_LEDCTRL|s_ram_addr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(0);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a12\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(1);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a13\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(2);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a14\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(3);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a15\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(4);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a32\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(5);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a33\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(6);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a34\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(7);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a35\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\(8);

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTADATAIN_bus\ <= (gnd & gnd & gnd & gnd & gnd & gnd & \U_PIFACE|s_output\(10) & \U_PIFACE|s_output\(9) & \U_PIFACE|s_output\(8));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTAADDR_bus\ <= (\U_MEMORY|waddr\(9) & \U_MEMORY|waddr\(8) & \U_MEMORY|waddr\(7) & \U_MEMORY|waddr\(6) & \U_MEMORY|waddr\(5) & \U_MEMORY|waddr\(4) & \U_MEMORY|waddr\(3) & 
\U_MEMORY|waddr\(2) & \U_MEMORY|waddr\(1) & \U_MEMORY|waddr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBADDR_bus\ <= (\U_LEDCTRL|s_ram_addr\(9) & \U_LEDCTRL|s_ram_addr\(8) & \U_LEDCTRL|s_ram_addr\(7) & \U_LEDCTRL|s_ram_addr\(6) & \U_LEDCTRL|s_ram_addr\(5) & \U_LEDCTRL|s_ram_addr\(4) & 
\U_LEDCTRL|s_ram_addr\(3) & \U_LEDCTRL|s_ram_addr\(2) & \U_LEDCTRL|s_ram_addr\(1) & \U_LEDCTRL|s_ram_addr\(0));

\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\(0);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a9\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\(1);
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a10\ <= \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\(2);

\U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \U_LEDCTRL|U_CLKDIV|clk_out~q\);

\U_PIFACE|count[5]~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \U_PIFACE|count\(5));

\rst~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \rst~combout\);

\clk_in~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk_in~input_o\);

\pi_ctl~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \pi_ctl~input_o\);
\ALT_INV_rst~clkctrl_outclk\ <= NOT \rst~clkctrl_outclk\;
\U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\ <= NOT \U_PIFACE|count[5]~clkctrl_outclk\;
\ALT_INV_rst~combout\ <= NOT \rst~combout\;
\U_LEDCTRL|ALT_INV_s_oe~0_combout\ <= NOT \U_LEDCTRL|s_oe~0_combout\;
\U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\ <= NOT \U_LEDCTRL|state.READ_PIXEL_DATA~q\;
\U_LEDCTRL|ALT_INV_s_led_addr\(3) <= NOT \U_LEDCTRL|s_led_addr\(3);
\U_LEDCTRL|ALT_INV_s_led_addr\(2) <= NOT \U_LEDCTRL|s_led_addr\(2);
\U_LEDCTRL|ALT_INV_s_led_addr\(1) <= NOT \U_LEDCTRL|s_led_addr\(1);
\U_LEDCTRL|ALT_INV_s_led_addr\(0) <= NOT \U_LEDCTRL|s_led_addr\(0);

-- Location: IOOBUF_X43_Y34_N16
\clk_out~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devoe => ww_devoe,
	o => \clk_out~output_o\);

-- Location: IOOBUF_X16_Y34_N16
\r1~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|s_rgb1\(2),
	devoe => ww_devoe,
	o => \r1~output_o\);

-- Location: IOOBUF_X34_Y34_N2
\r2~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|s_rgb2\(2),
	devoe => ww_devoe,
	o => \r2~output_o\);

-- Location: IOOBUF_X20_Y34_N9
\b1~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|s_rgb1\(0),
	devoe => ww_devoe,
	o => \b1~output_o\);

-- Location: IOOBUF_X31_Y34_N2
\b2~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|s_rgb2\(0),
	devoe => ww_devoe,
	o => \b2~output_o\);

-- Location: IOOBUF_X45_Y34_N16
\g1~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|s_rgb1\(1),
	devoe => ww_devoe,
	o => \g1~output_o\);

-- Location: IOOBUF_X40_Y34_N9
\g2~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|s_rgb2\(1),
	devoe => ww_devoe,
	o => \g2~output_o\);

-- Location: IOOBUF_X45_Y34_N9
\a~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|ALT_INV_s_led_addr\(0),
	devoe => ww_devoe,
	o => \a~output_o\);

-- Location: IOOBUF_X51_Y34_N16
\b~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|ALT_INV_s_led_addr\(1),
	devoe => ww_devoe,
	o => \b~output_o\);

-- Location: IOOBUF_X38_Y34_N2
\c~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|ALT_INV_s_led_addr\(2),
	devoe => ww_devoe,
	o => \c~output_o\);

-- Location: IOOBUF_X29_Y34_N16
\d~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|ALT_INV_s_led_addr\(3),
	devoe => ww_devoe,
	o => \d~output_o\);

-- Location: IOOBUF_X43_Y34_N23
\lat~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|state.LATCH~q\,
	devoe => ww_devoe,
	o => \lat~output_o\);

-- Location: IOOBUF_X51_Y34_N23
\oe~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \U_LEDCTRL|ALT_INV_s_oe~0_combout\,
	devoe => ww_devoe,
	o => \oe~output_o\);

-- Location: IOIBUF_X27_Y0_N22
\clk_in~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk_in,
	o => \clk_in~input_o\);

-- Location: CLKCTRL_G18
\clk_in~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk_in~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk_in~inputclkctrl_outclk\);

-- Location: LCCOMB_X1_Y16_N0
\U_LEDCTRL|U_CLKDIV|count~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|U_CLKDIV|count~0_combout\ = !\U_LEDCTRL|U_CLKDIV|count~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|U_CLKDIV|count~q\,
	combout => \U_LEDCTRL|U_CLKDIV|count~0_combout\);

-- Location: IOIBUF_X38_Y0_N1
\pi_rst~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_rst,
	o => \pi_rst~input_o\);

-- Location: IOIBUF_X53_Y14_N1
\rst_n~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rst_n,
	o => \rst_n~input_o\);

-- Location: LCCOMB_X48_Y14_N20
rst : cycloneive_lcell_comb
-- Equation(s):
-- \rst~combout\ = (\pi_rst~input_o\) # (!\rst_n~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \pi_rst~input_o\,
	datad => \rst_n~input_o\,
	combout => \rst~combout\);

-- Location: CLKCTRL_G6
\rst~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \rst~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \rst~clkctrl_outclk\);

-- Location: FF_X1_Y16_N1
\U_LEDCTRL|U_CLKDIV|count\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \U_LEDCTRL|U_CLKDIV|count~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|U_CLKDIV|count~q\);

-- Location: LCCOMB_X1_Y16_N14
\U_LEDCTRL|U_CLKDIV|clk_out~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|U_CLKDIV|clk_out~0_combout\ = !\U_LEDCTRL|U_CLKDIV|count~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_LEDCTRL|U_CLKDIV|count~q\,
	combout => \U_LEDCTRL|U_CLKDIV|clk_out~0_combout\);

-- Location: FF_X1_Y16_N15
\U_LEDCTRL|U_CLKDIV|clk_out\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_in~inputclkctrl_outclk\,
	d => \U_LEDCTRL|U_CLKDIV|clk_out~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|U_CLKDIV|clk_out~q\);

-- Location: CLKCTRL_G4
\U_LEDCTRL|U_CLKDIV|clk_out~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\);

-- Location: LCCOMB_X35_Y12_N0
\U_LEDCTRL|col_count[0]~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|col_count[0]~7_combout\ = \U_LEDCTRL|col_count\(0) $ (VCC)
-- \U_LEDCTRL|col_count[0]~8\ = CARRY(\U_LEDCTRL|col_count\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|col_count\(0),
	datad => VCC,
	combout => \U_LEDCTRL|col_count[0]~7_combout\,
	cout => \U_LEDCTRL|col_count[0]~8\);

-- Location: LCCOMB_X31_Y13_N2
\U_LEDCTRL|state.LATCH~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|state.LATCH~feeder_combout\ = \U_LEDCTRL|state.INCR_LED_ADDR~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|state.LATCH~feeder_combout\);

-- Location: FF_X31_Y13_N3
\U_LEDCTRL|state.LATCH\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|state.LATCH~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|state.LATCH~q\);

-- Location: LCCOMB_X31_Y13_N20
\U_LEDCTRL|state.INIT~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|state.INIT~0_combout\ = !\U_LEDCTRL|state.LATCH~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_LEDCTRL|state.LATCH~q\,
	combout => \U_LEDCTRL|state.INIT~0_combout\);

-- Location: FF_X31_Y13_N21
\U_LEDCTRL|state.INIT\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|state.INIT~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|state.INIT~q\);

-- Location: LCCOMB_X34_Y13_N2
\U_LEDCTRL|next_state.READ_PIXEL_DATA\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|next_state.READ_PIXEL_DATA~combout\ = (\U_LEDCTRL|state.INCR_RAM_ADDR~q\) # (!\U_LEDCTRL|state.INIT~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|state.INIT~q\,
	datad => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	combout => \U_LEDCTRL|next_state.READ_PIXEL_DATA~combout\);

-- Location: FF_X34_Y13_N3
\U_LEDCTRL|state.READ_PIXEL_DATA\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|next_state.READ_PIXEL_DATA~combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|state.READ_PIXEL_DATA~q\);

-- Location: LCCOMB_X35_Y12_N18
\U_LEDCTRL|next_state.INCR_LED_ADDR~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|next_state.INCR_LED_ADDR~0_combout\ = (\U_LEDCTRL|state.READ_PIXEL_DATA~q\ & \U_LEDCTRL|col_count\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|state.READ_PIXEL_DATA~q\,
	datad => \U_LEDCTRL|col_count\(6),
	combout => \U_LEDCTRL|next_state.INCR_LED_ADDR~0_combout\);

-- Location: FF_X35_Y12_N19
\U_LEDCTRL|state.INCR_LED_ADDR\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|next_state.INCR_LED_ADDR~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|state.INCR_LED_ADDR~q\);

-- Location: LCCOMB_X35_Y12_N16
\U_LEDCTRL|Selector6~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Selector6~0_combout\ = (\U_LEDCTRL|col_count\(0) & !\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|col_count\(0),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|Selector6~0_combout\);

-- Location: FF_X35_Y12_N1
\U_LEDCTRL|col_count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|col_count[0]~7_combout\,
	asdata => \U_LEDCTRL|Selector6~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|col_count\(0));

-- Location: LCCOMB_X35_Y12_N2
\U_LEDCTRL|col_count[1]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|col_count[1]~9_combout\ = (\U_LEDCTRL|col_count\(1) & (!\U_LEDCTRL|col_count[0]~8\)) # (!\U_LEDCTRL|col_count\(1) & ((\U_LEDCTRL|col_count[0]~8\) # (GND)))
-- \U_LEDCTRL|col_count[1]~10\ = CARRY((!\U_LEDCTRL|col_count[0]~8\) # (!\U_LEDCTRL|col_count\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|col_count\(1),
	datad => VCC,
	cin => \U_LEDCTRL|col_count[0]~8\,
	combout => \U_LEDCTRL|col_count[1]~9_combout\,
	cout => \U_LEDCTRL|col_count[1]~10\);

-- Location: LCCOMB_X35_Y12_N22
\U_LEDCTRL|Selector5~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Selector5~0_combout\ = (\U_LEDCTRL|col_count\(1) & !\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|col_count\(1),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|Selector5~0_combout\);

-- Location: FF_X35_Y12_N3
\U_LEDCTRL|col_count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|col_count[1]~9_combout\,
	asdata => \U_LEDCTRL|Selector5~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|col_count\(1));

-- Location: LCCOMB_X35_Y12_N4
\U_LEDCTRL|col_count[2]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|col_count[2]~11_combout\ = (\U_LEDCTRL|col_count\(2) & (\U_LEDCTRL|col_count[1]~10\ $ (GND))) # (!\U_LEDCTRL|col_count\(2) & (!\U_LEDCTRL|col_count[1]~10\ & VCC))
-- \U_LEDCTRL|col_count[2]~12\ = CARRY((\U_LEDCTRL|col_count\(2) & !\U_LEDCTRL|col_count[1]~10\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|col_count\(2),
	datad => VCC,
	cin => \U_LEDCTRL|col_count[1]~10\,
	combout => \U_LEDCTRL|col_count[2]~11_combout\,
	cout => \U_LEDCTRL|col_count[2]~12\);

-- Location: LCCOMB_X35_Y12_N24
\U_LEDCTRL|Selector4~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Selector4~0_combout\ = (\U_LEDCTRL|col_count\(2) & !\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|col_count\(2),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|Selector4~0_combout\);

-- Location: FF_X35_Y12_N5
\U_LEDCTRL|col_count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|col_count[2]~11_combout\,
	asdata => \U_LEDCTRL|Selector4~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|col_count\(2));

-- Location: LCCOMB_X35_Y12_N6
\U_LEDCTRL|col_count[3]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|col_count[3]~13_combout\ = (\U_LEDCTRL|col_count\(3) & (!\U_LEDCTRL|col_count[2]~12\)) # (!\U_LEDCTRL|col_count\(3) & ((\U_LEDCTRL|col_count[2]~12\) # (GND)))
-- \U_LEDCTRL|col_count[3]~14\ = CARRY((!\U_LEDCTRL|col_count[2]~12\) # (!\U_LEDCTRL|col_count\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|col_count\(3),
	datad => VCC,
	cin => \U_LEDCTRL|col_count[2]~12\,
	combout => \U_LEDCTRL|col_count[3]~13_combout\,
	cout => \U_LEDCTRL|col_count[3]~14\);

-- Location: LCCOMB_X35_Y12_N30
\U_LEDCTRL|Selector3~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Selector3~0_combout\ = (\U_LEDCTRL|col_count\(3) & !\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|col_count\(3),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|Selector3~0_combout\);

-- Location: FF_X35_Y12_N7
\U_LEDCTRL|col_count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|col_count[3]~13_combout\,
	asdata => \U_LEDCTRL|Selector3~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|col_count\(3));

-- Location: LCCOMB_X35_Y12_N8
\U_LEDCTRL|col_count[4]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|col_count[4]~15_combout\ = (\U_LEDCTRL|col_count\(4) & (\U_LEDCTRL|col_count[3]~14\ $ (GND))) # (!\U_LEDCTRL|col_count\(4) & (!\U_LEDCTRL|col_count[3]~14\ & VCC))
-- \U_LEDCTRL|col_count[4]~16\ = CARRY((\U_LEDCTRL|col_count\(4) & !\U_LEDCTRL|col_count[3]~14\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|col_count\(4),
	datad => VCC,
	cin => \U_LEDCTRL|col_count[3]~14\,
	combout => \U_LEDCTRL|col_count[4]~15_combout\,
	cout => \U_LEDCTRL|col_count[4]~16\);

-- Location: LCCOMB_X35_Y12_N20
\U_LEDCTRL|Selector2~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Selector2~0_combout\ = (\U_LEDCTRL|col_count\(4) & !\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|col_count\(4),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|Selector2~0_combout\);

-- Location: FF_X35_Y12_N9
\U_LEDCTRL|col_count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|col_count[4]~15_combout\,
	asdata => \U_LEDCTRL|Selector2~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|col_count\(4));

-- Location: LCCOMB_X35_Y12_N10
\U_LEDCTRL|col_count[5]~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|col_count[5]~17_combout\ = (\U_LEDCTRL|col_count\(5) & (!\U_LEDCTRL|col_count[4]~16\)) # (!\U_LEDCTRL|col_count\(5) & ((\U_LEDCTRL|col_count[4]~16\) # (GND)))
-- \U_LEDCTRL|col_count[5]~18\ = CARRY((!\U_LEDCTRL|col_count[4]~16\) # (!\U_LEDCTRL|col_count\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|col_count\(5),
	datad => VCC,
	cin => \U_LEDCTRL|col_count[4]~16\,
	combout => \U_LEDCTRL|col_count[5]~17_combout\,
	cout => \U_LEDCTRL|col_count[5]~18\);

-- Location: LCCOMB_X35_Y12_N26
\U_LEDCTRL|Selector1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Selector1~0_combout\ = (\U_LEDCTRL|col_count\(5) & !\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|col_count\(5),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|Selector1~0_combout\);

-- Location: FF_X35_Y12_N11
\U_LEDCTRL|col_count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|col_count[5]~17_combout\,
	asdata => \U_LEDCTRL|Selector1~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|col_count\(5));

-- Location: LCCOMB_X35_Y12_N12
\U_LEDCTRL|col_count[6]~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|col_count[6]~19_combout\ = \U_LEDCTRL|col_count[5]~18\ $ (!\U_LEDCTRL|col_count\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \U_LEDCTRL|col_count\(6),
	cin => \U_LEDCTRL|col_count[5]~18\,
	combout => \U_LEDCTRL|col_count[6]~19_combout\);

-- Location: LCCOMB_X35_Y12_N28
\U_LEDCTRL|Selector0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Selector0~0_combout\ = (\U_LEDCTRL|col_count\(6) & !\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|col_count\(6),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|Selector0~0_combout\);

-- Location: FF_X35_Y12_N13
\U_LEDCTRL|col_count[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|col_count[6]~19_combout\,
	asdata => \U_LEDCTRL|Selector0~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|col_count\(6));

-- Location: LCCOMB_X34_Y12_N4
\U_LEDCTRL|next_state.INCR_RAM_ADDR~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|next_state.INCR_RAM_ADDR~0_combout\ = (!\U_LEDCTRL|col_count\(6) & \U_LEDCTRL|state.READ_PIXEL_DATA~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|col_count\(6),
	datad => \U_LEDCTRL|state.READ_PIXEL_DATA~q\,
	combout => \U_LEDCTRL|next_state.INCR_RAM_ADDR~0_combout\);

-- Location: FF_X34_Y12_N5
\U_LEDCTRL|state.INCR_RAM_ADDR\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|next_state.INCR_RAM_ADDR~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|state.INCR_RAM_ADDR~q\);

-- Location: LCCOMB_X31_Y13_N4
\U_LEDCTRL|bpp_count[0]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[0]~9_combout\ = \U_LEDCTRL|bpp_count\(0) $ (VCC)
-- \U_LEDCTRL|bpp_count[0]~10\ = CARRY(\U_LEDCTRL|bpp_count\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(0),
	datad => VCC,
	combout => \U_LEDCTRL|bpp_count[0]~9_combout\,
	cout => \U_LEDCTRL|bpp_count[0]~10\);

-- Location: LCCOMB_X31_Y13_N30
\U_LEDCTRL|Equal1~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Equal1~0_combout\ = (\U_LEDCTRL|bpp_count\(1) & (!\U_LEDCTRL|bpp_count\(0) & (\U_LEDCTRL|bpp_count\(2) & \U_LEDCTRL|bpp_count\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(1),
	datab => \U_LEDCTRL|bpp_count\(0),
	datac => \U_LEDCTRL|bpp_count\(2),
	datad => \U_LEDCTRL|bpp_count\(7),
	combout => \U_LEDCTRL|Equal1~0_combout\);

-- Location: LCCOMB_X31_Y13_N0
\U_LEDCTRL|Equal1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Equal1~1_combout\ = (\U_LEDCTRL|bpp_count\(4) & (\U_LEDCTRL|bpp_count\(6) & (\U_LEDCTRL|bpp_count\(5) & \U_LEDCTRL|bpp_count\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(4),
	datab => \U_LEDCTRL|bpp_count\(6),
	datac => \U_LEDCTRL|bpp_count\(5),
	datad => \U_LEDCTRL|bpp_count\(3),
	combout => \U_LEDCTRL|Equal1~1_combout\);

-- Location: LCCOMB_X31_Y13_N22
\U_LEDCTRL|Equal1~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|Equal1~2_combout\ = (\U_LEDCTRL|Equal1~0_combout\ & \U_LEDCTRL|Equal1~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|Equal1~0_combout\,
	datad => \U_LEDCTRL|Equal1~1_combout\,
	combout => \U_LEDCTRL|Equal1~2_combout\);

-- Location: LCCOMB_X34_Y13_N28
\U_LEDCTRL|s_led_addr[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_led_addr[0]~0_combout\ = \U_LEDCTRL|s_led_addr\(0) $ (\U_LEDCTRL|state.INCR_LED_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_LEDCTRL|s_led_addr\(0),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|s_led_addr[0]~0_combout\);

-- Location: FF_X34_Y13_N29
\U_LEDCTRL|s_led_addr[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_led_addr[0]~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_led_addr\(0));

-- Location: LCCOMB_X34_Y13_N30
\U_LEDCTRL|s_led_addr[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_led_addr[1]~1_combout\ = \U_LEDCTRL|s_led_addr\(1) $ (((!\U_LEDCTRL|s_led_addr\(0) & \U_LEDCTRL|state.INCR_LED_ADDR~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|s_led_addr\(0),
	datac => \U_LEDCTRL|s_led_addr\(1),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|s_led_addr[1]~1_combout\);

-- Location: FF_X34_Y13_N31
\U_LEDCTRL|s_led_addr[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_led_addr[1]~1_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_led_addr\(1));

-- Location: LCCOMB_X34_Y13_N8
\U_LEDCTRL|s_led_addr[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_led_addr[2]~2_combout\ = \U_LEDCTRL|s_led_addr\(2) $ (((!\U_LEDCTRL|s_led_addr\(0) & (!\U_LEDCTRL|s_led_addr\(1) & \U_LEDCTRL|state.INCR_LED_ADDR~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|s_led_addr\(0),
	datab => \U_LEDCTRL|s_led_addr\(1),
	datac => \U_LEDCTRL|s_led_addr\(2),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|s_led_addr[2]~2_combout\);

-- Location: FF_X34_Y13_N9
\U_LEDCTRL|s_led_addr[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_led_addr[2]~2_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_led_addr\(2));

-- Location: LCCOMB_X34_Y13_N10
\U_LEDCTRL|bpp_count[7]~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[7]~8_combout\ = (!\U_LEDCTRL|s_led_addr\(2) & (!\U_LEDCTRL|s_led_addr\(1) & !\U_LEDCTRL|s_led_addr\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|s_led_addr\(2),
	datac => \U_LEDCTRL|s_led_addr\(1),
	datad => \U_LEDCTRL|s_led_addr\(0),
	combout => \U_LEDCTRL|bpp_count[7]~8_combout\);

-- Location: LCCOMB_X31_Y13_N24
\U_LEDCTRL|s_led_addr[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_led_addr[3]~3_combout\ = \U_LEDCTRL|s_led_addr\(3) $ (((\U_LEDCTRL|bpp_count[7]~8_combout\ & \U_LEDCTRL|state.INCR_LED_ADDR~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count[7]~8_combout\,
	datac => \U_LEDCTRL|s_led_addr\(3),
	datad => \U_LEDCTRL|state.INCR_LED_ADDR~q\,
	combout => \U_LEDCTRL|s_led_addr[3]~3_combout\);

-- Location: FF_X31_Y13_N25
\U_LEDCTRL|s_led_addr[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_led_addr[3]~3_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_led_addr\(3));

-- Location: LCCOMB_X31_Y13_N28
\U_LEDCTRL|bpp_count[7]~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[7]~25_combout\ = (!\U_LEDCTRL|s_led_addr\(3) & (!\U_LEDCTRL|state.INIT~q\ & \U_LEDCTRL|bpp_count[7]~8_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|s_led_addr\(3),
	datac => \U_LEDCTRL|state.INIT~q\,
	datad => \U_LEDCTRL|bpp_count[7]~8_combout\,
	combout => \U_LEDCTRL|bpp_count[7]~25_combout\);

-- Location: FF_X31_Y13_N5
\U_LEDCTRL|bpp_count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[0]~9_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(0));

-- Location: LCCOMB_X31_Y13_N6
\U_LEDCTRL|bpp_count[1]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[1]~11_combout\ = (\U_LEDCTRL|bpp_count\(1) & (!\U_LEDCTRL|bpp_count[0]~10\)) # (!\U_LEDCTRL|bpp_count\(1) & ((\U_LEDCTRL|bpp_count[0]~10\) # (GND)))
-- \U_LEDCTRL|bpp_count[1]~12\ = CARRY((!\U_LEDCTRL|bpp_count[0]~10\) # (!\U_LEDCTRL|bpp_count\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(1),
	datad => VCC,
	cin => \U_LEDCTRL|bpp_count[0]~10\,
	combout => \U_LEDCTRL|bpp_count[1]~11_combout\,
	cout => \U_LEDCTRL|bpp_count[1]~12\);

-- Location: FF_X31_Y13_N7
\U_LEDCTRL|bpp_count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[1]~11_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(1));

-- Location: LCCOMB_X31_Y13_N8
\U_LEDCTRL|bpp_count[2]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[2]~13_combout\ = (\U_LEDCTRL|bpp_count\(2) & (\U_LEDCTRL|bpp_count[1]~12\ $ (GND))) # (!\U_LEDCTRL|bpp_count\(2) & (!\U_LEDCTRL|bpp_count[1]~12\ & VCC))
-- \U_LEDCTRL|bpp_count[2]~14\ = CARRY((\U_LEDCTRL|bpp_count\(2) & !\U_LEDCTRL|bpp_count[1]~12\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(2),
	datad => VCC,
	cin => \U_LEDCTRL|bpp_count[1]~12\,
	combout => \U_LEDCTRL|bpp_count[2]~13_combout\,
	cout => \U_LEDCTRL|bpp_count[2]~14\);

-- Location: FF_X31_Y13_N9
\U_LEDCTRL|bpp_count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[2]~13_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(2));

-- Location: LCCOMB_X31_Y13_N10
\U_LEDCTRL|bpp_count[3]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[3]~15_combout\ = (\U_LEDCTRL|bpp_count\(3) & (!\U_LEDCTRL|bpp_count[2]~14\)) # (!\U_LEDCTRL|bpp_count\(3) & ((\U_LEDCTRL|bpp_count[2]~14\) # (GND)))
-- \U_LEDCTRL|bpp_count[3]~16\ = CARRY((!\U_LEDCTRL|bpp_count[2]~14\) # (!\U_LEDCTRL|bpp_count\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(3),
	datad => VCC,
	cin => \U_LEDCTRL|bpp_count[2]~14\,
	combout => \U_LEDCTRL|bpp_count[3]~15_combout\,
	cout => \U_LEDCTRL|bpp_count[3]~16\);

-- Location: FF_X31_Y13_N11
\U_LEDCTRL|bpp_count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[3]~15_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(3));

-- Location: LCCOMB_X31_Y13_N12
\U_LEDCTRL|bpp_count[4]~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[4]~17_combout\ = (\U_LEDCTRL|bpp_count\(4) & (\U_LEDCTRL|bpp_count[3]~16\ $ (GND))) # (!\U_LEDCTRL|bpp_count\(4) & (!\U_LEDCTRL|bpp_count[3]~16\ & VCC))
-- \U_LEDCTRL|bpp_count[4]~18\ = CARRY((\U_LEDCTRL|bpp_count\(4) & !\U_LEDCTRL|bpp_count[3]~16\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(4),
	datad => VCC,
	cin => \U_LEDCTRL|bpp_count[3]~16\,
	combout => \U_LEDCTRL|bpp_count[4]~17_combout\,
	cout => \U_LEDCTRL|bpp_count[4]~18\);

-- Location: FF_X31_Y13_N13
\U_LEDCTRL|bpp_count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[4]~17_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(4));

-- Location: LCCOMB_X31_Y13_N14
\U_LEDCTRL|bpp_count[5]~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[5]~19_combout\ = (\U_LEDCTRL|bpp_count\(5) & (!\U_LEDCTRL|bpp_count[4]~18\)) # (!\U_LEDCTRL|bpp_count\(5) & ((\U_LEDCTRL|bpp_count[4]~18\) # (GND)))
-- \U_LEDCTRL|bpp_count[5]~20\ = CARRY((!\U_LEDCTRL|bpp_count[4]~18\) # (!\U_LEDCTRL|bpp_count\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(5),
	datad => VCC,
	cin => \U_LEDCTRL|bpp_count[4]~18\,
	combout => \U_LEDCTRL|bpp_count[5]~19_combout\,
	cout => \U_LEDCTRL|bpp_count[5]~20\);

-- Location: FF_X31_Y13_N15
\U_LEDCTRL|bpp_count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[5]~19_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(5));

-- Location: LCCOMB_X31_Y13_N16
\U_LEDCTRL|bpp_count[6]~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[6]~21_combout\ = (\U_LEDCTRL|bpp_count\(6) & (\U_LEDCTRL|bpp_count[5]~20\ $ (GND))) # (!\U_LEDCTRL|bpp_count\(6) & (!\U_LEDCTRL|bpp_count[5]~20\ & VCC))
-- \U_LEDCTRL|bpp_count[6]~22\ = CARRY((\U_LEDCTRL|bpp_count\(6) & !\U_LEDCTRL|bpp_count[5]~20\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(6),
	datad => VCC,
	cin => \U_LEDCTRL|bpp_count[5]~20\,
	combout => \U_LEDCTRL|bpp_count[6]~21_combout\,
	cout => \U_LEDCTRL|bpp_count[6]~22\);

-- Location: FF_X31_Y13_N17
\U_LEDCTRL|bpp_count[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[6]~21_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(6));

-- Location: LCCOMB_X31_Y13_N18
\U_LEDCTRL|bpp_count[7]~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|bpp_count[7]~23_combout\ = \U_LEDCTRL|bpp_count[6]~22\ $ (\U_LEDCTRL|bpp_count\(7))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \U_LEDCTRL|bpp_count\(7),
	cin => \U_LEDCTRL|bpp_count[6]~22\,
	combout => \U_LEDCTRL|bpp_count[7]~23_combout\);

-- Location: FF_X31_Y13_N19
\U_LEDCTRL|bpp_count[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|bpp_count[7]~23_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|Equal1~2_combout\,
	ena => \U_LEDCTRL|bpp_count[7]~25_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|bpp_count\(7));

-- Location: IOIBUF_X34_Y0_N15
\pi_ctl~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_ctl,
	o => \pi_ctl~input_o\);

-- Location: CLKCTRL_G17
\pi_ctl~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \pi_ctl~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \pi_ctl~inputclkctrl_outclk\);

-- Location: LCCOMB_X25_Y33_N8
\U_PIFACE|count[4]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|count[4]~1_combout\ = !\U_PIFACE|count\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_PIFACE|count\(5),
	combout => \U_PIFACE|count[4]~1_combout\);

-- Location: LCCOMB_X25_Y33_N12
\U_PIFACE|count[4]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|count[4]~feeder_combout\ = \U_PIFACE|count[4]~1_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_PIFACE|count[4]~1_combout\,
	combout => \U_PIFACE|count[4]~feeder_combout\);

-- Location: FF_X25_Y33_N13
\U_PIFACE|count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|count[4]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|count\(4));

-- Location: LCCOMB_X25_Y33_N20
\U_PIFACE|count[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|count[3]~feeder_combout\ = \U_PIFACE|count\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|count\(4),
	combout => \U_PIFACE|count[3]~feeder_combout\);

-- Location: FF_X25_Y33_N21
\U_PIFACE|count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|count[3]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|count\(3));

-- Location: LCCOMB_X25_Y33_N18
\U_PIFACE|count[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|count[2]~feeder_combout\ = \U_PIFACE|count\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|count\(3),
	combout => \U_PIFACE|count[2]~feeder_combout\);

-- Location: FF_X25_Y33_N19
\U_PIFACE|count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|count[2]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|count\(2));

-- Location: LCCOMB_X25_Y33_N10
\U_PIFACE|count[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|count[1]~feeder_combout\ = \U_PIFACE|count\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|count\(2),
	combout => \U_PIFACE|count[1]~feeder_combout\);

-- Location: FF_X25_Y33_N11
\U_PIFACE|count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|count[1]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|count\(1));

-- Location: LCCOMB_X25_Y33_N16
\U_PIFACE|count[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|count[0]~feeder_combout\ = \U_PIFACE|count\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|count\(1),
	combout => \U_PIFACE|count[0]~feeder_combout\);

-- Location: FF_X25_Y33_N17
\U_PIFACE|count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|count[0]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|count\(0));

-- Location: LCCOMB_X25_Y33_N6
\U_PIFACE|count[5]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|count[5]~0_combout\ = !\U_PIFACE|count\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|count\(0),
	combout => \U_PIFACE|count[5]~0_combout\);

-- Location: FF_X25_Y33_N7
\U_PIFACE|count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|count[5]~0_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|count\(5));

-- Location: CLKCTRL_G12
\U_PIFACE|count[5]~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \U_PIFACE|count[5]~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \U_PIFACE|count[5]~clkctrl_outclk\);

-- Location: IOIBUF_X36_Y0_N22
\pi_dat[7]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(7),
	o => \pi_dat[7]~input_o\);

-- Location: LCCOMB_X32_Y14_N12
\U_PIFACE|s_output[7]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[7]~feeder_combout\ = \pi_dat[7]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pi_dat[7]~input_o\,
	combout => \U_PIFACE|s_output[7]~feeder_combout\);

-- Location: FF_X32_Y14_N13
\U_PIFACE|s_output[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[7]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(7));

-- Location: LCCOMB_X32_Y14_N0
\U_PIFACE|s_output[15]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[15]~feeder_combout\ = \U_PIFACE|s_output\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(7),
	combout => \U_PIFACE|s_output[15]~feeder_combout\);

-- Location: FF_X32_Y14_N1
\U_PIFACE|s_output[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[15]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(15));

-- Location: LCCOMB_X32_Y14_N28
\U_PIFACE|s_output[23]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[23]~feeder_combout\ = \U_PIFACE|s_output\(15)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(15),
	combout => \U_PIFACE|s_output[23]~feeder_combout\);

-- Location: FF_X32_Y14_N29
\U_PIFACE|s_output[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[23]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(23));

-- Location: LCCOMB_X34_Y16_N4
\U_MEMORY|waddr[0]~27\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[0]~27_combout\ = !\U_MEMORY|waddr\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_MEMORY|waddr\(0),
	combout => \U_MEMORY|waddr[0]~27_combout\);

-- Location: FF_X34_Y16_N5
\U_MEMORY|waddr[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[0]~27_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(0));

-- Location: LCCOMB_X34_Y16_N8
\U_MEMORY|waddr[1]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[1]~9_combout\ = (\U_MEMORY|waddr\(1) & (\U_MEMORY|waddr\(0) $ (VCC))) # (!\U_MEMORY|waddr\(1) & (\U_MEMORY|waddr\(0) & VCC))
-- \U_MEMORY|waddr[1]~10\ = CARRY((\U_MEMORY|waddr\(1) & \U_MEMORY|waddr\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|waddr\(1),
	datab => \U_MEMORY|waddr\(0),
	datad => VCC,
	combout => \U_MEMORY|waddr[1]~9_combout\,
	cout => \U_MEMORY|waddr[1]~10\);

-- Location: FF_X34_Y16_N9
\U_MEMORY|waddr[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[1]~9_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(1));

-- Location: LCCOMB_X34_Y16_N10
\U_MEMORY|waddr[2]~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[2]~11_combout\ = (\U_MEMORY|waddr\(2) & (!\U_MEMORY|waddr[1]~10\)) # (!\U_MEMORY|waddr\(2) & ((\U_MEMORY|waddr[1]~10\) # (GND)))
-- \U_MEMORY|waddr[2]~12\ = CARRY((!\U_MEMORY|waddr[1]~10\) # (!\U_MEMORY|waddr\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|waddr\(2),
	datad => VCC,
	cin => \U_MEMORY|waddr[1]~10\,
	combout => \U_MEMORY|waddr[2]~11_combout\,
	cout => \U_MEMORY|waddr[2]~12\);

-- Location: FF_X34_Y16_N11
\U_MEMORY|waddr[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[2]~11_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(2));

-- Location: LCCOMB_X34_Y16_N12
\U_MEMORY|waddr[3]~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[3]~13_combout\ = (\U_MEMORY|waddr\(3) & (\U_MEMORY|waddr[2]~12\ $ (GND))) # (!\U_MEMORY|waddr\(3) & (!\U_MEMORY|waddr[2]~12\ & VCC))
-- \U_MEMORY|waddr[3]~14\ = CARRY((\U_MEMORY|waddr\(3) & !\U_MEMORY|waddr[2]~12\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_MEMORY|waddr\(3),
	datad => VCC,
	cin => \U_MEMORY|waddr[2]~12\,
	combout => \U_MEMORY|waddr[3]~13_combout\,
	cout => \U_MEMORY|waddr[3]~14\);

-- Location: FF_X34_Y16_N13
\U_MEMORY|waddr[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[3]~13_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(3));

-- Location: LCCOMB_X34_Y16_N14
\U_MEMORY|waddr[4]~15\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[4]~15_combout\ = (\U_MEMORY|waddr\(4) & (!\U_MEMORY|waddr[3]~14\)) # (!\U_MEMORY|waddr\(4) & ((\U_MEMORY|waddr[3]~14\) # (GND)))
-- \U_MEMORY|waddr[4]~16\ = CARRY((!\U_MEMORY|waddr[3]~14\) # (!\U_MEMORY|waddr\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_MEMORY|waddr\(4),
	datad => VCC,
	cin => \U_MEMORY|waddr[3]~14\,
	combout => \U_MEMORY|waddr[4]~15_combout\,
	cout => \U_MEMORY|waddr[4]~16\);

-- Location: FF_X34_Y16_N15
\U_MEMORY|waddr[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[4]~15_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(4));

-- Location: LCCOMB_X34_Y16_N16
\U_MEMORY|waddr[5]~17\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[5]~17_combout\ = (\U_MEMORY|waddr\(5) & (\U_MEMORY|waddr[4]~16\ $ (GND))) # (!\U_MEMORY|waddr\(5) & (!\U_MEMORY|waddr[4]~16\ & VCC))
-- \U_MEMORY|waddr[5]~18\ = CARRY((\U_MEMORY|waddr\(5) & !\U_MEMORY|waddr[4]~16\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_MEMORY|waddr\(5),
	datad => VCC,
	cin => \U_MEMORY|waddr[4]~16\,
	combout => \U_MEMORY|waddr[5]~17_combout\,
	cout => \U_MEMORY|waddr[5]~18\);

-- Location: FF_X34_Y16_N17
\U_MEMORY|waddr[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[5]~17_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(5));

-- Location: LCCOMB_X34_Y16_N18
\U_MEMORY|waddr[6]~19\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[6]~19_combout\ = (\U_MEMORY|waddr\(6) & (!\U_MEMORY|waddr[5]~18\)) # (!\U_MEMORY|waddr\(6) & ((\U_MEMORY|waddr[5]~18\) # (GND)))
-- \U_MEMORY|waddr[6]~20\ = CARRY((!\U_MEMORY|waddr[5]~18\) # (!\U_MEMORY|waddr\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_MEMORY|waddr\(6),
	datad => VCC,
	cin => \U_MEMORY|waddr[5]~18\,
	combout => \U_MEMORY|waddr[6]~19_combout\,
	cout => \U_MEMORY|waddr[6]~20\);

-- Location: FF_X34_Y16_N19
\U_MEMORY|waddr[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[6]~19_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(6));

-- Location: LCCOMB_X34_Y16_N20
\U_MEMORY|waddr[7]~21\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[7]~21_combout\ = (\U_MEMORY|waddr\(7) & (\U_MEMORY|waddr[6]~20\ $ (GND))) # (!\U_MEMORY|waddr\(7) & (!\U_MEMORY|waddr[6]~20\ & VCC))
-- \U_MEMORY|waddr[7]~22\ = CARRY((\U_MEMORY|waddr\(7) & !\U_MEMORY|waddr[6]~20\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_MEMORY|waddr\(7),
	datad => VCC,
	cin => \U_MEMORY|waddr[6]~20\,
	combout => \U_MEMORY|waddr[7]~21_combout\,
	cout => \U_MEMORY|waddr[7]~22\);

-- Location: FF_X34_Y16_N21
\U_MEMORY|waddr[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[7]~21_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(7));

-- Location: LCCOMB_X34_Y16_N22
\U_MEMORY|waddr[8]~23\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[8]~23_combout\ = (\U_MEMORY|waddr\(8) & (!\U_MEMORY|waddr[7]~22\)) # (!\U_MEMORY|waddr\(8) & ((\U_MEMORY|waddr[7]~22\) # (GND)))
-- \U_MEMORY|waddr[8]~24\ = CARRY((!\U_MEMORY|waddr[7]~22\) # (!\U_MEMORY|waddr\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|waddr\(8),
	datad => VCC,
	cin => \U_MEMORY|waddr[7]~22\,
	combout => \U_MEMORY|waddr[8]~23_combout\,
	cout => \U_MEMORY|waddr[8]~24\);

-- Location: FF_X34_Y16_N23
\U_MEMORY|waddr[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[8]~23_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(8));

-- Location: LCCOMB_X34_Y16_N24
\U_MEMORY|waddr[9]~25\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_MEMORY|waddr[9]~25_combout\ = \U_MEMORY|waddr[8]~24\ $ (!\U_MEMORY|waddr\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \U_MEMORY|waddr\(9),
	cin => \U_MEMORY|waddr[8]~24\,
	combout => \U_MEMORY|waddr[9]~25_combout\);

-- Location: FF_X34_Y16_N25
\U_MEMORY|waddr[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	d => \U_MEMORY|waddr[9]~25_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_MEMORY|waddr\(9));

-- Location: LCCOMB_X34_Y12_N6
\U_LEDCTRL|s_ram_addr[0]~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[0]~9_combout\ = \U_LEDCTRL|state.INCR_RAM_ADDR~q\ $ (\U_LEDCTRL|s_ram_addr\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	datac => \U_LEDCTRL|s_ram_addr\(0),
	combout => \U_LEDCTRL|s_ram_addr[0]~9_combout\);

-- Location: FF_X34_Y12_N7
\U_LEDCTRL|s_ram_addr[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[0]~9_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(0));

-- Location: LCCOMB_X34_Y12_N8
\U_LEDCTRL|s_ram_addr[1]~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[1]~10_combout\ = (\U_LEDCTRL|s_ram_addr\(0) & (\U_LEDCTRL|s_ram_addr\(1) $ (VCC))) # (!\U_LEDCTRL|s_ram_addr\(0) & (\U_LEDCTRL|s_ram_addr\(1) & VCC))
-- \U_LEDCTRL|s_ram_addr[1]~11\ = CARRY((\U_LEDCTRL|s_ram_addr\(0) & \U_LEDCTRL|s_ram_addr\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|s_ram_addr\(0),
	datab => \U_LEDCTRL|s_ram_addr\(1),
	datad => VCC,
	combout => \U_LEDCTRL|s_ram_addr[1]~10_combout\,
	cout => \U_LEDCTRL|s_ram_addr[1]~11\);

-- Location: FF_X34_Y12_N9
\U_LEDCTRL|s_ram_addr[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[1]~10_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(1));

-- Location: LCCOMB_X34_Y12_N10
\U_LEDCTRL|s_ram_addr[2]~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[2]~12_combout\ = (\U_LEDCTRL|s_ram_addr\(2) & (!\U_LEDCTRL|s_ram_addr[1]~11\)) # (!\U_LEDCTRL|s_ram_addr\(2) & ((\U_LEDCTRL|s_ram_addr[1]~11\) # (GND)))
-- \U_LEDCTRL|s_ram_addr[2]~13\ = CARRY((!\U_LEDCTRL|s_ram_addr[1]~11\) # (!\U_LEDCTRL|s_ram_addr\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|s_ram_addr\(2),
	datad => VCC,
	cin => \U_LEDCTRL|s_ram_addr[1]~11\,
	combout => \U_LEDCTRL|s_ram_addr[2]~12_combout\,
	cout => \U_LEDCTRL|s_ram_addr[2]~13\);

-- Location: FF_X34_Y12_N11
\U_LEDCTRL|s_ram_addr[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[2]~12_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(2));

-- Location: LCCOMB_X34_Y12_N12
\U_LEDCTRL|s_ram_addr[3]~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[3]~14_combout\ = (\U_LEDCTRL|s_ram_addr\(3) & (\U_LEDCTRL|s_ram_addr[2]~13\ $ (GND))) # (!\U_LEDCTRL|s_ram_addr\(3) & (!\U_LEDCTRL|s_ram_addr[2]~13\ & VCC))
-- \U_LEDCTRL|s_ram_addr[3]~15\ = CARRY((\U_LEDCTRL|s_ram_addr\(3) & !\U_LEDCTRL|s_ram_addr[2]~13\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|s_ram_addr\(3),
	datad => VCC,
	cin => \U_LEDCTRL|s_ram_addr[2]~13\,
	combout => \U_LEDCTRL|s_ram_addr[3]~14_combout\,
	cout => \U_LEDCTRL|s_ram_addr[3]~15\);

-- Location: FF_X34_Y12_N13
\U_LEDCTRL|s_ram_addr[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[3]~14_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(3));

-- Location: LCCOMB_X34_Y12_N14
\U_LEDCTRL|s_ram_addr[4]~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[4]~16_combout\ = (\U_LEDCTRL|s_ram_addr\(4) & (!\U_LEDCTRL|s_ram_addr[3]~15\)) # (!\U_LEDCTRL|s_ram_addr\(4) & ((\U_LEDCTRL|s_ram_addr[3]~15\) # (GND)))
-- \U_LEDCTRL|s_ram_addr[4]~17\ = CARRY((!\U_LEDCTRL|s_ram_addr[3]~15\) # (!\U_LEDCTRL|s_ram_addr\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|s_ram_addr\(4),
	datad => VCC,
	cin => \U_LEDCTRL|s_ram_addr[3]~15\,
	combout => \U_LEDCTRL|s_ram_addr[4]~16_combout\,
	cout => \U_LEDCTRL|s_ram_addr[4]~17\);

-- Location: FF_X34_Y12_N15
\U_LEDCTRL|s_ram_addr[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[4]~16_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(4));

-- Location: LCCOMB_X34_Y12_N16
\U_LEDCTRL|s_ram_addr[5]~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[5]~18_combout\ = (\U_LEDCTRL|s_ram_addr\(5) & (\U_LEDCTRL|s_ram_addr[4]~17\ $ (GND))) # (!\U_LEDCTRL|s_ram_addr\(5) & (!\U_LEDCTRL|s_ram_addr[4]~17\ & VCC))
-- \U_LEDCTRL|s_ram_addr[5]~19\ = CARRY((\U_LEDCTRL|s_ram_addr\(5) & !\U_LEDCTRL|s_ram_addr[4]~17\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|s_ram_addr\(5),
	datad => VCC,
	cin => \U_LEDCTRL|s_ram_addr[4]~17\,
	combout => \U_LEDCTRL|s_ram_addr[5]~18_combout\,
	cout => \U_LEDCTRL|s_ram_addr[5]~19\);

-- Location: FF_X34_Y12_N17
\U_LEDCTRL|s_ram_addr[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[5]~18_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(5));

-- Location: LCCOMB_X34_Y12_N18
\U_LEDCTRL|s_ram_addr[6]~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[6]~20_combout\ = (\U_LEDCTRL|s_ram_addr\(6) & (!\U_LEDCTRL|s_ram_addr[5]~19\)) # (!\U_LEDCTRL|s_ram_addr\(6) & ((\U_LEDCTRL|s_ram_addr[5]~19\) # (GND)))
-- \U_LEDCTRL|s_ram_addr[6]~21\ = CARRY((!\U_LEDCTRL|s_ram_addr[5]~19\) # (!\U_LEDCTRL|s_ram_addr\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|s_ram_addr\(6),
	datad => VCC,
	cin => \U_LEDCTRL|s_ram_addr[5]~19\,
	combout => \U_LEDCTRL|s_ram_addr[6]~20_combout\,
	cout => \U_LEDCTRL|s_ram_addr[6]~21\);

-- Location: FF_X34_Y12_N19
\U_LEDCTRL|s_ram_addr[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[6]~20_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(6));

-- Location: LCCOMB_X34_Y12_N20
\U_LEDCTRL|s_ram_addr[7]~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[7]~22_combout\ = (\U_LEDCTRL|s_ram_addr\(7) & (\U_LEDCTRL|s_ram_addr[6]~21\ $ (GND))) # (!\U_LEDCTRL|s_ram_addr\(7) & (!\U_LEDCTRL|s_ram_addr[6]~21\ & VCC))
-- \U_LEDCTRL|s_ram_addr[7]~23\ = CARRY((\U_LEDCTRL|s_ram_addr\(7) & !\U_LEDCTRL|s_ram_addr[6]~21\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|s_ram_addr\(7),
	datad => VCC,
	cin => \U_LEDCTRL|s_ram_addr[6]~21\,
	combout => \U_LEDCTRL|s_ram_addr[7]~22_combout\,
	cout => \U_LEDCTRL|s_ram_addr[7]~23\);

-- Location: FF_X34_Y12_N21
\U_LEDCTRL|s_ram_addr[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[7]~22_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(7));

-- Location: LCCOMB_X34_Y12_N22
\U_LEDCTRL|s_ram_addr[8]~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[8]~24_combout\ = (\U_LEDCTRL|s_ram_addr\(8) & (!\U_LEDCTRL|s_ram_addr[7]~23\)) # (!\U_LEDCTRL|s_ram_addr\(8) & ((\U_LEDCTRL|s_ram_addr[7]~23\) # (GND)))
-- \U_LEDCTRL|s_ram_addr[8]~25\ = CARRY((!\U_LEDCTRL|s_ram_addr[7]~23\) # (!\U_LEDCTRL|s_ram_addr\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|s_ram_addr\(8),
	datad => VCC,
	cin => \U_LEDCTRL|s_ram_addr[7]~23\,
	combout => \U_LEDCTRL|s_ram_addr[8]~24_combout\,
	cout => \U_LEDCTRL|s_ram_addr[8]~25\);

-- Location: FF_X34_Y12_N23
\U_LEDCTRL|s_ram_addr[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[8]~24_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(8));

-- Location: LCCOMB_X34_Y12_N24
\U_LEDCTRL|s_ram_addr[9]~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_ram_addr[9]~26_combout\ = \U_LEDCTRL|s_ram_addr[8]~25\ $ (!\U_LEDCTRL|s_ram_addr\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \U_LEDCTRL|s_ram_addr\(9),
	cin => \U_LEDCTRL|s_ram_addr[8]~25\,
	combout => \U_LEDCTRL|s_ram_addr[9]~26_combout\);

-- Location: FF_X34_Y12_N25
\U_LEDCTRL|s_ram_addr[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|s_ram_addr[9]~26_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	ena => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_ram_addr\(9));

-- Location: IOIBUF_X53_Y21_N22
\pi_dat[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(0),
	o => \pi_dat[0]~input_o\);

-- Location: LCCOMB_X34_Y14_N26
\U_PIFACE|s_output[0]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[0]~feeder_combout\ = \pi_dat[0]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pi_dat[0]~input_o\,
	combout => \U_PIFACE|s_output[0]~feeder_combout\);

-- Location: FF_X34_Y14_N27
\U_PIFACE|s_output[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[0]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(0));

-- Location: LCCOMB_X34_Y14_N22
\U_PIFACE|s_output[8]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[8]~feeder_combout\ = \U_PIFACE|s_output\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(0),
	combout => \U_PIFACE|s_output[8]~feeder_combout\);

-- Location: FF_X34_Y14_N23
\U_PIFACE|s_output[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[8]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(8));

-- Location: LCCOMB_X34_Y14_N30
\U_PIFACE|s_output[16]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[16]~feeder_combout\ = \U_PIFACE|s_output\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(8),
	combout => \U_PIFACE|s_output[16]~feeder_combout\);

-- Location: FF_X34_Y14_N31
\U_PIFACE|s_output[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[16]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(16));

-- Location: LCCOMB_X34_Y14_N6
\U_PIFACE|s_output[24]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[24]~feeder_combout\ = \U_PIFACE|s_output\(16)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(16),
	combout => \U_PIFACE|s_output[24]~feeder_combout\);

-- Location: FF_X34_Y14_N7
\U_PIFACE|s_output[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[24]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(24));

-- Location: LCCOMB_X34_Y14_N2
\U_PIFACE|s_output[32]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[32]~feeder_combout\ = \U_PIFACE|s_output\(24)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(24),
	combout => \U_PIFACE|s_output[32]~feeder_combout\);

-- Location: FF_X34_Y14_N3
\U_PIFACE|s_output[32]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[32]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(32));

-- Location: LCCOMB_X34_Y14_N14
\U_PIFACE|s_output[40]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[40]~feeder_combout\ = \U_PIFACE|s_output\(32)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(32),
	combout => \U_PIFACE|s_output[40]~feeder_combout\);

-- Location: FF_X34_Y14_N15
\U_PIFACE|s_output[40]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[40]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(40));

-- Location: IOIBUF_X45_Y0_N15
\pi_dat[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(1),
	o => \pi_dat[1]~input_o\);

-- Location: LCCOMB_X31_Y11_N8
\U_PIFACE|s_output[1]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[1]~feeder_combout\ = \pi_dat[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pi_dat[1]~input_o\,
	combout => \U_PIFACE|s_output[1]~feeder_combout\);

-- Location: FF_X31_Y11_N9
\U_PIFACE|s_output[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[1]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(1));

-- Location: LCCOMB_X32_Y11_N4
\U_PIFACE|s_output[9]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[9]~feeder_combout\ = \U_PIFACE|s_output\(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(1),
	combout => \U_PIFACE|s_output[9]~feeder_combout\);

-- Location: FF_X32_Y11_N5
\U_PIFACE|s_output[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[9]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(9));

-- Location: LCCOMB_X32_Y11_N2
\U_PIFACE|s_output[17]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[17]~feeder_combout\ = \U_PIFACE|s_output\(9)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(9),
	combout => \U_PIFACE|s_output[17]~feeder_combout\);

-- Location: FF_X32_Y11_N3
\U_PIFACE|s_output[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[17]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(17));

-- Location: LCCOMB_X32_Y11_N0
\U_PIFACE|s_output[25]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[25]~feeder_combout\ = \U_PIFACE|s_output\(17)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(17),
	combout => \U_PIFACE|s_output[25]~feeder_combout\);

-- Location: FF_X32_Y11_N1
\U_PIFACE|s_output[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[25]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(25));

-- Location: LCCOMB_X32_Y11_N30
\U_PIFACE|s_output[33]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[33]~feeder_combout\ = \U_PIFACE|s_output\(25)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(25),
	combout => \U_PIFACE|s_output[33]~feeder_combout\);

-- Location: FF_X32_Y11_N31
\U_PIFACE|s_output[33]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[33]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(33));

-- Location: LCCOMB_X32_Y11_N8
\U_PIFACE|s_output[41]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[41]~feeder_combout\ = \U_PIFACE|s_output\(33)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(33),
	combout => \U_PIFACE|s_output[41]~feeder_combout\);

-- Location: FF_X32_Y11_N9
\U_PIFACE|s_output[41]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[41]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(41));

-- Location: IOIBUF_X45_Y0_N22
\pi_dat[2]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(2),
	o => \pi_dat[2]~input_o\);

-- Location: LCCOMB_X32_Y12_N2
\U_PIFACE|s_output[2]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[2]~feeder_combout\ = \pi_dat[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pi_dat[2]~input_o\,
	combout => \U_PIFACE|s_output[2]~feeder_combout\);

-- Location: FF_X32_Y12_N3
\U_PIFACE|s_output[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[2]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(2));

-- Location: LCCOMB_X32_Y12_N6
\U_PIFACE|s_output[10]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[10]~feeder_combout\ = \U_PIFACE|s_output\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(2),
	combout => \U_PIFACE|s_output[10]~feeder_combout\);

-- Location: FF_X32_Y12_N7
\U_PIFACE|s_output[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[10]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(10));

-- Location: LCCOMB_X32_Y12_N18
\U_PIFACE|s_output[18]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[18]~feeder_combout\ = \U_PIFACE|s_output\(10)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(10),
	combout => \U_PIFACE|s_output[18]~feeder_combout\);

-- Location: FF_X32_Y12_N19
\U_PIFACE|s_output[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[18]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(18));

-- Location: LCCOMB_X32_Y12_N0
\U_PIFACE|s_output[26]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[26]~feeder_combout\ = \U_PIFACE|s_output\(18)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(18),
	combout => \U_PIFACE|s_output[26]~feeder_combout\);

-- Location: FF_X32_Y12_N1
\U_PIFACE|s_output[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[26]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(26));

-- Location: LCCOMB_X32_Y12_N24
\U_PIFACE|s_output[34]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[34]~feeder_combout\ = \U_PIFACE|s_output\(26)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(26),
	combout => \U_PIFACE|s_output[34]~feeder_combout\);

-- Location: FF_X32_Y12_N25
\U_PIFACE|s_output[34]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[34]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(34));

-- Location: LCCOMB_X32_Y12_N28
\U_PIFACE|s_output[42]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[42]~feeder_combout\ = \U_PIFACE|s_output\(34)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(34),
	combout => \U_PIFACE|s_output[42]~feeder_combout\);

-- Location: FF_X32_Y12_N29
\U_PIFACE|s_output[42]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[42]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(42));

-- Location: IOIBUF_X40_Y0_N15
\pi_dat[3]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(3),
	o => \pi_dat[3]~input_o\);

-- Location: LCCOMB_X34_Y15_N30
\U_PIFACE|s_output[3]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[3]~feeder_combout\ = \pi_dat[3]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pi_dat[3]~input_o\,
	combout => \U_PIFACE|s_output[3]~feeder_combout\);

-- Location: FF_X34_Y15_N31
\U_PIFACE|s_output[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[3]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(3));

-- Location: LCCOMB_X34_Y15_N14
\U_PIFACE|s_output[11]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[11]~feeder_combout\ = \U_PIFACE|s_output\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(3),
	combout => \U_PIFACE|s_output[11]~feeder_combout\);

-- Location: FF_X34_Y15_N15
\U_PIFACE|s_output[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[11]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(11));

-- Location: LCCOMB_X34_Y15_N18
\U_PIFACE|s_output[19]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[19]~feeder_combout\ = \U_PIFACE|s_output\(11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(11),
	combout => \U_PIFACE|s_output[19]~feeder_combout\);

-- Location: FF_X34_Y15_N19
\U_PIFACE|s_output[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[19]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(19));

-- Location: LCCOMB_X34_Y15_N4
\U_PIFACE|s_output[27]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[27]~feeder_combout\ = \U_PIFACE|s_output\(19)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(19),
	combout => \U_PIFACE|s_output[27]~feeder_combout\);

-- Location: FF_X34_Y15_N5
\U_PIFACE|s_output[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[27]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(27));

-- Location: LCCOMB_X34_Y15_N16
\U_PIFACE|s_output[35]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[35]~feeder_combout\ = \U_PIFACE|s_output\(27)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(27),
	combout => \U_PIFACE|s_output[35]~feeder_combout\);

-- Location: FF_X34_Y15_N17
\U_PIFACE|s_output[35]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[35]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(35));

-- Location: LCCOMB_X34_Y15_N28
\U_PIFACE|s_output[43]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[43]~feeder_combout\ = \U_PIFACE|s_output\(35)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(35),
	combout => \U_PIFACE|s_output[43]~feeder_combout\);

-- Location: FF_X34_Y15_N29
\U_PIFACE|s_output[43]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[43]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(43));

-- Location: IOIBUF_X40_Y0_N22
\pi_dat[4]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(4),
	o => \pi_dat[4]~input_o\);

-- Location: FF_X32_Y14_N7
\U_PIFACE|s_output[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	asdata => \pi_dat[4]~input_o\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(4));

-- Location: LCCOMB_X32_Y14_N22
\U_PIFACE|s_output[12]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[12]~feeder_combout\ = \U_PIFACE|s_output\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(4),
	combout => \U_PIFACE|s_output[12]~feeder_combout\);

-- Location: FF_X32_Y14_N23
\U_PIFACE|s_output[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[12]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(12));

-- Location: LCCOMB_X32_Y14_N30
\U_PIFACE|s_output[20]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[20]~feeder_combout\ = \U_PIFACE|s_output\(12)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(12),
	combout => \U_PIFACE|s_output[20]~feeder_combout\);

-- Location: FF_X32_Y14_N31
\U_PIFACE|s_output[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[20]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(20));

-- Location: LCCOMB_X32_Y14_N18
\U_PIFACE|s_output[28]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[28]~feeder_combout\ = \U_PIFACE|s_output\(20)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(20),
	combout => \U_PIFACE|s_output[28]~feeder_combout\);

-- Location: FF_X32_Y14_N19
\U_PIFACE|s_output[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[28]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(28));

-- Location: LCCOMB_X32_Y14_N10
\U_PIFACE|s_output[36]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[36]~feeder_combout\ = \U_PIFACE|s_output\(28)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(28),
	combout => \U_PIFACE|s_output[36]~feeder_combout\);

-- Location: FF_X32_Y14_N11
\U_PIFACE|s_output[36]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[36]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(36));

-- Location: LCCOMB_X32_Y14_N14
\U_PIFACE|s_output[44]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[44]~feeder_combout\ = \U_PIFACE|s_output\(36)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(36),
	combout => \U_PIFACE|s_output[44]~feeder_combout\);

-- Location: FF_X32_Y14_N15
\U_PIFACE|s_output[44]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[44]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(44));

-- Location: IOIBUF_X36_Y0_N8
\pi_dat[5]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(5),
	o => \pi_dat[5]~input_o\);

-- Location: LCCOMB_X34_Y11_N14
\U_PIFACE|s_output[5]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[5]~feeder_combout\ = \pi_dat[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pi_dat[5]~input_o\,
	combout => \U_PIFACE|s_output[5]~feeder_combout\);

-- Location: FF_X34_Y11_N15
\U_PIFACE|s_output[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[5]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(5));

-- Location: LCCOMB_X34_Y11_N2
\U_PIFACE|s_output[13]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[13]~feeder_combout\ = \U_PIFACE|s_output\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(5),
	combout => \U_PIFACE|s_output[13]~feeder_combout\);

-- Location: FF_X34_Y11_N3
\U_PIFACE|s_output[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[13]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(13));

-- Location: LCCOMB_X34_Y11_N22
\U_PIFACE|s_output[21]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[21]~feeder_combout\ = \U_PIFACE|s_output\(13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(13),
	combout => \U_PIFACE|s_output[21]~feeder_combout\);

-- Location: FF_X34_Y11_N23
\U_PIFACE|s_output[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[21]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(21));

-- Location: LCCOMB_X34_Y11_N0
\U_PIFACE|s_output[29]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[29]~feeder_combout\ = \U_PIFACE|s_output\(21)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(21),
	combout => \U_PIFACE|s_output[29]~feeder_combout\);

-- Location: FF_X34_Y11_N1
\U_PIFACE|s_output[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[29]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(29));

-- Location: LCCOMB_X34_Y11_N16
\U_PIFACE|s_output[37]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[37]~feeder_combout\ = \U_PIFACE|s_output\(29)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(29),
	combout => \U_PIFACE|s_output[37]~feeder_combout\);

-- Location: FF_X34_Y11_N17
\U_PIFACE|s_output[37]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[37]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(37));

-- Location: LCCOMB_X34_Y11_N24
\U_PIFACE|s_output[45]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[45]~feeder_combout\ = \U_PIFACE|s_output\(37)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(37),
	combout => \U_PIFACE|s_output[45]~feeder_combout\);

-- Location: FF_X34_Y11_N25
\U_PIFACE|s_output[45]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[45]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(45));

-- Location: IOIBUF_X36_Y0_N15
\pi_dat[6]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_pi_dat(6),
	o => \pi_dat[6]~input_o\);

-- Location: LCCOMB_X34_Y14_N8
\U_PIFACE|s_output[6]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[6]~feeder_combout\ = \pi_dat[6]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \pi_dat[6]~input_o\,
	combout => \U_PIFACE|s_output[6]~feeder_combout\);

-- Location: FF_X34_Y14_N9
\U_PIFACE|s_output[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[6]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(6));

-- Location: LCCOMB_X34_Y14_N0
\U_PIFACE|s_output[14]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[14]~feeder_combout\ = \U_PIFACE|s_output\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \U_PIFACE|s_output\(6),
	combout => \U_PIFACE|s_output[14]~feeder_combout\);

-- Location: FF_X34_Y14_N1
\U_PIFACE|s_output[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[14]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(14));

-- Location: LCCOMB_X34_Y14_N20
\U_PIFACE|s_output[22]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[22]~feeder_combout\ = \U_PIFACE|s_output\(14)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(14),
	combout => \U_PIFACE|s_output[22]~feeder_combout\);

-- Location: FF_X34_Y14_N21
\U_PIFACE|s_output[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[22]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(22));

-- Location: LCCOMB_X34_Y14_N28
\U_PIFACE|s_output[30]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[30]~feeder_combout\ = \U_PIFACE|s_output\(22)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(22),
	combout => \U_PIFACE|s_output[30]~feeder_combout\);

-- Location: FF_X34_Y14_N29
\U_PIFACE|s_output[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[30]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(30));

-- Location: LCCOMB_X34_Y14_N24
\U_PIFACE|s_output[38]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[38]~feeder_combout\ = \U_PIFACE|s_output\(30)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(30),
	combout => \U_PIFACE|s_output[38]~feeder_combout\);

-- Location: FF_X34_Y14_N25
\U_PIFACE|s_output[38]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[38]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(38));

-- Location: LCCOMB_X34_Y14_N12
\U_PIFACE|s_output[46]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[46]~feeder_combout\ = \U_PIFACE|s_output\(38)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(38),
	combout => \U_PIFACE|s_output[46]~feeder_combout\);

-- Location: FF_X34_Y14_N13
\U_PIFACE|s_output[46]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[46]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(46));

-- Location: LCCOMB_X32_Y14_N16
\U_PIFACE|s_output[31]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[31]~feeder_combout\ = \U_PIFACE|s_output\(23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(23),
	combout => \U_PIFACE|s_output[31]~feeder_combout\);

-- Location: FF_X32_Y14_N17
\U_PIFACE|s_output[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[31]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(31));

-- Location: LCCOMB_X32_Y14_N24
\U_PIFACE|s_output[39]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[39]~feeder_combout\ = \U_PIFACE|s_output\(31)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(31),
	combout => \U_PIFACE|s_output[39]~feeder_combout\);

-- Location: FF_X32_Y14_N25
\U_PIFACE|s_output[39]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[39]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(39));

-- Location: LCCOMB_X32_Y14_N8
\U_PIFACE|s_output[47]~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_PIFACE|s_output[47]~feeder_combout\ = \U_PIFACE|s_output\(39)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \U_PIFACE|s_output\(39),
	combout => \U_PIFACE|s_output[47]~feeder_combout\);

-- Location: FF_X32_Y14_N9
\U_PIFACE|s_output[47]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \pi_ctl~inputclkctrl_outclk\,
	d => \U_PIFACE|s_output[47]~feeder_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_PIFACE|s_output\(47));

-- Location: M9K_X33_Y11_N0
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "memory:U_MEMORY|altsyncram:ram_block_rtl_0|altsyncram_u8d1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 23,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 48,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 23,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 48,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \ALT_INV_rst~combout\,
	portbre => VCC,
	clk0 => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	clk1 => \clk_in~inputclkctrl_outclk\,
	ena0 => \ALT_INV_rst~combout\,
	portadatain => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTADATAIN_bus\,
	portaaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTAADDR_bus\,
	portbaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X32_Y11_N12
\U_LEDCTRL|LessThan0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~1_cout\ = CARRY((!\U_LEDCTRL|bpp_count\(0) & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a40\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(0),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a40\,
	datad => VCC,
	cout => \U_LEDCTRL|LessThan0~1_cout\);

-- Location: LCCOMB_X32_Y11_N14
\U_LEDCTRL|LessThan0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~3_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a41\ & (\U_LEDCTRL|bpp_count\(1) & !\U_LEDCTRL|LessThan0~1_cout\)) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a41\ & ((\U_LEDCTRL|bpp_count\(1)) # 
-- (!\U_LEDCTRL|LessThan0~1_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a41\,
	datab => \U_LEDCTRL|bpp_count\(1),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan0~1_cout\,
	cout => \U_LEDCTRL|LessThan0~3_cout\);

-- Location: LCCOMB_X32_Y11_N16
\U_LEDCTRL|LessThan0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~5_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a42\ & ((!\U_LEDCTRL|LessThan0~3_cout\) # (!\U_LEDCTRL|bpp_count\(2)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a42\ & (!\U_LEDCTRL|bpp_count\(2) & 
-- !\U_LEDCTRL|LessThan0~3_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a42\,
	datab => \U_LEDCTRL|bpp_count\(2),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan0~3_cout\,
	cout => \U_LEDCTRL|LessThan0~5_cout\);

-- Location: LCCOMB_X32_Y11_N18
\U_LEDCTRL|LessThan0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~7_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a43\ & (\U_LEDCTRL|bpp_count\(3) & !\U_LEDCTRL|LessThan0~5_cout\)) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a43\ & ((\U_LEDCTRL|bpp_count\(3)) # 
-- (!\U_LEDCTRL|LessThan0~5_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a43\,
	datab => \U_LEDCTRL|bpp_count\(3),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan0~5_cout\,
	cout => \U_LEDCTRL|LessThan0~7_cout\);

-- Location: LCCOMB_X32_Y11_N20
\U_LEDCTRL|LessThan0~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~9_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a44\ & ((!\U_LEDCTRL|LessThan0~7_cout\) # (!\U_LEDCTRL|bpp_count\(4)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a44\ & (!\U_LEDCTRL|bpp_count\(4) & 
-- !\U_LEDCTRL|LessThan0~7_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a44\,
	datab => \U_LEDCTRL|bpp_count\(4),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan0~7_cout\,
	cout => \U_LEDCTRL|LessThan0~9_cout\);

-- Location: LCCOMB_X32_Y11_N22
\U_LEDCTRL|LessThan0~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~11_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a45\ & (\U_LEDCTRL|bpp_count\(5) & !\U_LEDCTRL|LessThan0~9_cout\)) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a45\ & ((\U_LEDCTRL|bpp_count\(5)) # 
-- (!\U_LEDCTRL|LessThan0~9_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a45\,
	datab => \U_LEDCTRL|bpp_count\(5),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan0~9_cout\,
	cout => \U_LEDCTRL|LessThan0~11_cout\);

-- Location: LCCOMB_X32_Y11_N24
\U_LEDCTRL|LessThan0~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~13_cout\ = CARRY((\U_LEDCTRL|bpp_count\(6) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a46\ & !\U_LEDCTRL|LessThan0~11_cout\)) # (!\U_LEDCTRL|bpp_count\(6) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a46\) # 
-- (!\U_LEDCTRL|LessThan0~11_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(6),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a46\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan0~11_cout\,
	cout => \U_LEDCTRL|LessThan0~13_cout\);

-- Location: LCCOMB_X32_Y11_N26
\U_LEDCTRL|LessThan0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan0~14_combout\ = (\U_LEDCTRL|bpp_count\(7) & (\U_LEDCTRL|LessThan0~13_cout\ & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a47\)) # (!\U_LEDCTRL|bpp_count\(7) & ((\U_LEDCTRL|LessThan0~13_cout\) # 
-- (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a47\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(7),
	datad => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a47\,
	cin => \U_LEDCTRL|LessThan0~13_cout\,
	combout => \U_LEDCTRL|LessThan0~14_combout\);

-- Location: FF_X32_Y11_N27
\U_LEDCTRL|s_rgb1[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|LessThan0~14_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_rgb1\(2));

-- Location: M9K_X33_Y13_N0
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "memory:U_MEMORY|altsyncram:ram_block_rtl_0|altsyncram_u8d1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 16,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 48,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 16,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 48,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \ALT_INV_rst~combout\,
	portbre => VCC,
	clk0 => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	clk1 => \clk_in~inputclkctrl_outclk\,
	ena0 => \ALT_INV_rst~combout\,
	portadatain => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTADATAIN_bus\,
	portaaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTAADDR_bus\,
	portbaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X32_Y13_N4
\U_LEDCTRL|LessThan3~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~1_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\ & !\U_LEDCTRL|bpp_count\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a16~portbdataout\,
	datab => \U_LEDCTRL|bpp_count\(0),
	datad => VCC,
	cout => \U_LEDCTRL|LessThan3~1_cout\);

-- Location: LCCOMB_X32_Y13_N6
\U_LEDCTRL|LessThan3~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~3_cout\ = CARRY((\U_LEDCTRL|bpp_count\(1) & ((!\U_LEDCTRL|LessThan3~1_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a17\))) # (!\U_LEDCTRL|bpp_count\(1) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a17\ & 
-- !\U_LEDCTRL|LessThan3~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(1),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a17\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan3~1_cout\,
	cout => \U_LEDCTRL|LessThan3~3_cout\);

-- Location: LCCOMB_X32_Y13_N8
\U_LEDCTRL|LessThan3~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~5_cout\ = CARRY((\U_LEDCTRL|bpp_count\(2) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a18\ & !\U_LEDCTRL|LessThan3~3_cout\)) # (!\U_LEDCTRL|bpp_count\(2) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a18\) # 
-- (!\U_LEDCTRL|LessThan3~3_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(2),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a18\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan3~3_cout\,
	cout => \U_LEDCTRL|LessThan3~5_cout\);

-- Location: LCCOMB_X32_Y13_N10
\U_LEDCTRL|LessThan3~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~7_cout\ = CARRY((\U_LEDCTRL|bpp_count\(3) & ((!\U_LEDCTRL|LessThan3~5_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a19\))) # (!\U_LEDCTRL|bpp_count\(3) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a19\ & 
-- !\U_LEDCTRL|LessThan3~5_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(3),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a19\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan3~5_cout\,
	cout => \U_LEDCTRL|LessThan3~7_cout\);

-- Location: LCCOMB_X32_Y13_N12
\U_LEDCTRL|LessThan3~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~9_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a20\ & ((!\U_LEDCTRL|LessThan3~7_cout\) # (!\U_LEDCTRL|bpp_count\(4)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a20\ & (!\U_LEDCTRL|bpp_count\(4) & 
-- !\U_LEDCTRL|LessThan3~7_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a20\,
	datab => \U_LEDCTRL|bpp_count\(4),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan3~7_cout\,
	cout => \U_LEDCTRL|LessThan3~9_cout\);

-- Location: LCCOMB_X32_Y13_N14
\U_LEDCTRL|LessThan3~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~11_cout\ = CARRY((\U_LEDCTRL|bpp_count\(5) & ((!\U_LEDCTRL|LessThan3~9_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a21\))) # (!\U_LEDCTRL|bpp_count\(5) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a21\ & 
-- !\U_LEDCTRL|LessThan3~9_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(5),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a21\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan3~9_cout\,
	cout => \U_LEDCTRL|LessThan3~11_cout\);

-- Location: LCCOMB_X32_Y13_N16
\U_LEDCTRL|LessThan3~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~13_cout\ = CARRY((\U_LEDCTRL|bpp_count\(6) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a22\ & !\U_LEDCTRL|LessThan3~11_cout\)) # (!\U_LEDCTRL|bpp_count\(6) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a22\) # 
-- (!\U_LEDCTRL|LessThan3~11_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(6),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a22\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan3~11_cout\,
	cout => \U_LEDCTRL|LessThan3~13_cout\);

-- Location: LCCOMB_X32_Y13_N18
\U_LEDCTRL|LessThan3~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan3~14_combout\ = (\U_LEDCTRL|bpp_count\(7) & (\U_LEDCTRL|LessThan3~13_cout\ & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\)) # (!\U_LEDCTRL|bpp_count\(7) & ((\U_LEDCTRL|LessThan3~13_cout\) # 
-- (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(7),
	datad => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a23~portbdataout\,
	cin => \U_LEDCTRL|LessThan3~13_cout\,
	combout => \U_LEDCTRL|LessThan3~14_combout\);

-- Location: FF_X32_Y13_N19
\U_LEDCTRL|s_rgb2[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|LessThan3~14_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_rgb2\(2));

-- Location: M9K_X33_Y12_N0
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "memory:U_MEMORY|altsyncram:ram_block_rtl_0|altsyncram_u8d1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 5,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 48,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 5,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 48,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \ALT_INV_rst~combout\,
	portbre => VCC,
	clk0 => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	clk1 => \clk_in~inputclkctrl_outclk\,
	ena0 => \ALT_INV_rst~combout\,
	portadatain => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTADATAIN_bus\,
	portaaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTAADDR_bus\,
	portbaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X34_Y13_N12
\U_LEDCTRL|LessThan2~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~1_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a24\ & !\U_LEDCTRL|bpp_count\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a24\,
	datab => \U_LEDCTRL|bpp_count\(0),
	datad => VCC,
	cout => \U_LEDCTRL|LessThan2~1_cout\);

-- Location: LCCOMB_X34_Y13_N14
\U_LEDCTRL|LessThan2~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~3_cout\ = CARRY((\U_LEDCTRL|bpp_count\(1) & ((!\U_LEDCTRL|LessThan2~1_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a25\))) # (!\U_LEDCTRL|bpp_count\(1) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a25\ & 
-- !\U_LEDCTRL|LessThan2~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(1),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a25\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan2~1_cout\,
	cout => \U_LEDCTRL|LessThan2~3_cout\);

-- Location: LCCOMB_X34_Y13_N16
\U_LEDCTRL|LessThan2~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~5_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a26\ & ((!\U_LEDCTRL|LessThan2~3_cout\) # (!\U_LEDCTRL|bpp_count\(2)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a26\ & (!\U_LEDCTRL|bpp_count\(2) & 
-- !\U_LEDCTRL|LessThan2~3_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a26\,
	datab => \U_LEDCTRL|bpp_count\(2),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan2~3_cout\,
	cout => \U_LEDCTRL|LessThan2~5_cout\);

-- Location: LCCOMB_X34_Y13_N18
\U_LEDCTRL|LessThan2~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~7_cout\ = CARRY((\U_LEDCTRL|bpp_count\(3) & ((!\U_LEDCTRL|LessThan2~5_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a27\))) # (!\U_LEDCTRL|bpp_count\(3) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a27\ & 
-- !\U_LEDCTRL|LessThan2~5_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(3),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a27\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan2~5_cout\,
	cout => \U_LEDCTRL|LessThan2~7_cout\);

-- Location: LCCOMB_X34_Y13_N20
\U_LEDCTRL|LessThan2~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~9_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a28\ & ((!\U_LEDCTRL|LessThan2~7_cout\) # (!\U_LEDCTRL|bpp_count\(4)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a28\ & (!\U_LEDCTRL|bpp_count\(4) & 
-- !\U_LEDCTRL|LessThan2~7_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a28\,
	datab => \U_LEDCTRL|bpp_count\(4),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan2~7_cout\,
	cout => \U_LEDCTRL|LessThan2~9_cout\);

-- Location: LCCOMB_X34_Y13_N22
\U_LEDCTRL|LessThan2~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~11_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a29\ & (\U_LEDCTRL|bpp_count\(5) & !\U_LEDCTRL|LessThan2~9_cout\)) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a29\ & ((\U_LEDCTRL|bpp_count\(5)) # 
-- (!\U_LEDCTRL|LessThan2~9_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a29\,
	datab => \U_LEDCTRL|bpp_count\(5),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan2~9_cout\,
	cout => \U_LEDCTRL|LessThan2~11_cout\);

-- Location: LCCOMB_X34_Y13_N24
\U_LEDCTRL|LessThan2~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~13_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a30\ & ((!\U_LEDCTRL|LessThan2~11_cout\) # (!\U_LEDCTRL|bpp_count\(6)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a30\ & (!\U_LEDCTRL|bpp_count\(6) & 
-- !\U_LEDCTRL|LessThan2~11_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a30\,
	datab => \U_LEDCTRL|bpp_count\(6),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan2~11_cout\,
	cout => \U_LEDCTRL|LessThan2~13_cout\);

-- Location: LCCOMB_X34_Y13_N26
\U_LEDCTRL|LessThan2~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan2~14_combout\ = (\U_LEDCTRL|bpp_count\(7) & (\U_LEDCTRL|LessThan2~13_cout\ & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a31\)) # (!\U_LEDCTRL|bpp_count\(7) & ((\U_LEDCTRL|LessThan2~13_cout\) # 
-- (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a31\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010101010000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(7),
	datad => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a31\,
	cin => \U_LEDCTRL|LessThan2~13_cout\,
	combout => \U_LEDCTRL|LessThan2~14_combout\);

-- Location: FF_X34_Y13_N27
\U_LEDCTRL|s_rgb1[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|LessThan2~14_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_rgb1\(0));

-- Location: M9K_X33_Y14_N0
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "memory:U_MEMORY|altsyncram:ram_block_rtl_0|altsyncram_u8d1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 48,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 48,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \ALT_INV_rst~combout\,
	portbre => VCC,
	clk0 => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	clk1 => \clk_in~inputclkctrl_outclk\,
	ena0 => \ALT_INV_rst~combout\,
	portadatain => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTADATAIN_bus\,
	portaaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTAADDR_bus\,
	portbaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X31_Y14_N8
\U_LEDCTRL|LessThan5~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~1_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\ & !\U_LEDCTRL|bpp_count\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a0~portbdataout\,
	datab => \U_LEDCTRL|bpp_count\(0),
	datad => VCC,
	cout => \U_LEDCTRL|LessThan5~1_cout\);

-- Location: LCCOMB_X31_Y14_N10
\U_LEDCTRL|LessThan5~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~3_cout\ = CARRY((\U_LEDCTRL|bpp_count\(1) & ((!\U_LEDCTRL|LessThan5~1_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a1\))) # (!\U_LEDCTRL|bpp_count\(1) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a1\ & 
-- !\U_LEDCTRL|LessThan5~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(1),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a1\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan5~1_cout\,
	cout => \U_LEDCTRL|LessThan5~3_cout\);

-- Location: LCCOMB_X31_Y14_N12
\U_LEDCTRL|LessThan5~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~5_cout\ = CARRY((\U_LEDCTRL|bpp_count\(2) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a2\ & !\U_LEDCTRL|LessThan5~3_cout\)) # (!\U_LEDCTRL|bpp_count\(2) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a2\) # 
-- (!\U_LEDCTRL|LessThan5~3_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(2),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a2\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan5~3_cout\,
	cout => \U_LEDCTRL|LessThan5~5_cout\);

-- Location: LCCOMB_X31_Y14_N14
\U_LEDCTRL|LessThan5~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~7_cout\ = CARRY((\U_LEDCTRL|bpp_count\(3) & ((!\U_LEDCTRL|LessThan5~5_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a3\))) # (!\U_LEDCTRL|bpp_count\(3) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a3\ & 
-- !\U_LEDCTRL|LessThan5~5_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(3),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a3\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan5~5_cout\,
	cout => \U_LEDCTRL|LessThan5~7_cout\);

-- Location: LCCOMB_X31_Y14_N16
\U_LEDCTRL|LessThan5~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~9_cout\ = CARRY((\U_LEDCTRL|bpp_count\(4) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a4\ & !\U_LEDCTRL|LessThan5~7_cout\)) # (!\U_LEDCTRL|bpp_count\(4) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a4\) # 
-- (!\U_LEDCTRL|LessThan5~7_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(4),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a4\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan5~7_cout\,
	cout => \U_LEDCTRL|LessThan5~9_cout\);

-- Location: LCCOMB_X31_Y14_N18
\U_LEDCTRL|LessThan5~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~11_cout\ = CARRY((\U_LEDCTRL|bpp_count\(5) & ((!\U_LEDCTRL|LessThan5~9_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\))) # (!\U_LEDCTRL|bpp_count\(5) & 
-- (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\ & !\U_LEDCTRL|LessThan5~9_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(5),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a5~portbdataout\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan5~9_cout\,
	cout => \U_LEDCTRL|LessThan5~11_cout\);

-- Location: LCCOMB_X31_Y14_N20
\U_LEDCTRL|LessThan5~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~13_cout\ = CARRY((\U_LEDCTRL|bpp_count\(6) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a6\ & !\U_LEDCTRL|LessThan5~11_cout\)) # (!\U_LEDCTRL|bpp_count\(6) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a6\) # 
-- (!\U_LEDCTRL|LessThan5~11_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(6),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a6\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan5~11_cout\,
	cout => \U_LEDCTRL|LessThan5~13_cout\);

-- Location: LCCOMB_X31_Y14_N22
\U_LEDCTRL|LessThan5~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan5~14_combout\ = (\U_LEDCTRL|bpp_count\(7) & (\U_LEDCTRL|LessThan5~13_cout\ & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a7\)) # (!\U_LEDCTRL|bpp_count\(7) & ((\U_LEDCTRL|LessThan5~13_cout\) # 
-- (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a7\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(7),
	datad => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a7\,
	cin => \U_LEDCTRL|LessThan5~13_cout\,
	combout => \U_LEDCTRL|LessThan5~14_combout\);

-- Location: FF_X31_Y14_N23
\U_LEDCTRL|s_rgb2[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|LessThan5~14_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_rgb2\(0));

-- Location: M9K_X33_Y15_N0
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "memory:U_MEMORY|altsyncram:ram_block_rtl_0|altsyncram_u8d1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 11,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 48,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 11,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 48,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \ALT_INV_rst~combout\,
	portbre => VCC,
	clk0 => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	clk1 => \clk_in~inputclkctrl_outclk\,
	ena0 => \ALT_INV_rst~combout\,
	portadatain => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTADATAIN_bus\,
	portaaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTAADDR_bus\,
	portbaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X32_Y15_N10
\U_LEDCTRL|LessThan1~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~1_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a32\ & !\U_LEDCTRL|bpp_count\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000100010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a32\,
	datab => \U_LEDCTRL|bpp_count\(0),
	datad => VCC,
	cout => \U_LEDCTRL|LessThan1~1_cout\);

-- Location: LCCOMB_X32_Y15_N12
\U_LEDCTRL|LessThan1~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~3_cout\ = CARRY((\U_LEDCTRL|bpp_count\(1) & ((!\U_LEDCTRL|LessThan1~1_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a33\))) # (!\U_LEDCTRL|bpp_count\(1) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a33\ & 
-- !\U_LEDCTRL|LessThan1~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(1),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a33\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan1~1_cout\,
	cout => \U_LEDCTRL|LessThan1~3_cout\);

-- Location: LCCOMB_X32_Y15_N14
\U_LEDCTRL|LessThan1~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~5_cout\ = CARRY((\U_LEDCTRL|bpp_count\(2) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a34\ & !\U_LEDCTRL|LessThan1~3_cout\)) # (!\U_LEDCTRL|bpp_count\(2) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a34\) # 
-- (!\U_LEDCTRL|LessThan1~3_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(2),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a34\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan1~3_cout\,
	cout => \U_LEDCTRL|LessThan1~5_cout\);

-- Location: LCCOMB_X32_Y15_N16
\U_LEDCTRL|LessThan1~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~7_cout\ = CARRY((\U_LEDCTRL|bpp_count\(3) & ((!\U_LEDCTRL|LessThan1~5_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a35\))) # (!\U_LEDCTRL|bpp_count\(3) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a35\ & 
-- !\U_LEDCTRL|LessThan1~5_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(3),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a35\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan1~5_cout\,
	cout => \U_LEDCTRL|LessThan1~7_cout\);

-- Location: LCCOMB_X32_Y15_N18
\U_LEDCTRL|LessThan1~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~9_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a36\ & ((!\U_LEDCTRL|LessThan1~7_cout\) # (!\U_LEDCTRL|bpp_count\(4)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a36\ & (!\U_LEDCTRL|bpp_count\(4) & 
-- !\U_LEDCTRL|LessThan1~7_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a36\,
	datab => \U_LEDCTRL|bpp_count\(4),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan1~7_cout\,
	cout => \U_LEDCTRL|LessThan1~9_cout\);

-- Location: LCCOMB_X32_Y15_N20
\U_LEDCTRL|LessThan1~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~11_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a37\ & (\U_LEDCTRL|bpp_count\(5) & !\U_LEDCTRL|LessThan1~9_cout\)) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a37\ & ((\U_LEDCTRL|bpp_count\(5)) # 
-- (!\U_LEDCTRL|LessThan1~9_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a37\,
	datab => \U_LEDCTRL|bpp_count\(5),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan1~9_cout\,
	cout => \U_LEDCTRL|LessThan1~11_cout\);

-- Location: LCCOMB_X32_Y15_N22
\U_LEDCTRL|LessThan1~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~13_cout\ = CARRY((\U_LEDCTRL|bpp_count\(6) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a38\ & !\U_LEDCTRL|LessThan1~11_cout\)) # (!\U_LEDCTRL|bpp_count\(6) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a38\) # 
-- (!\U_LEDCTRL|LessThan1~11_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(6),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a38\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan1~11_cout\,
	cout => \U_LEDCTRL|LessThan1~13_cout\);

-- Location: LCCOMB_X32_Y15_N24
\U_LEDCTRL|LessThan1~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan1~14_combout\ = (\U_LEDCTRL|bpp_count\(7) & (\U_LEDCTRL|LessThan1~13_cout\ & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a39\)) # (!\U_LEDCTRL|bpp_count\(7) & ((\U_LEDCTRL|LessThan1~13_cout\) # 
-- (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a39\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111010101010000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(7),
	datad => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a39\,
	cin => \U_LEDCTRL|LessThan1~13_cout\,
	combout => \U_LEDCTRL|LessThan1~14_combout\);

-- Location: FF_X32_Y15_N25
\U_LEDCTRL|s_rgb1[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|LessThan1~14_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_rgb1\(1));

-- Location: M9K_X33_Y16_N0
\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8\ : cycloneive_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "memory:U_MEMORY|altsyncram:ram_block_rtl_0|altsyncram_u8d1:auto_generated|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 8,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 48,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "none",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "none",
	port_b_data_out_clock => "none",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 8,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 48,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \ALT_INV_rst~combout\,
	portbre => VCC,
	clk0 => \U_PIFACE|ALT_INV_count[5]~clkctrl_outclk\,
	clk1 => \clk_in~inputclkctrl_outclk\,
	ena0 => \ALT_INV_rst~combout\,
	portadatain => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTADATAIN_bus\,
	portaaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTAADDR_bus\,
	portbaddr => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X32_Y16_N2
\U_LEDCTRL|LessThan4~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~1_cout\ = CARRY((!\U_LEDCTRL|bpp_count\(0) & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001000100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(0),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a8~portbdataout\,
	datad => VCC,
	cout => \U_LEDCTRL|LessThan4~1_cout\);

-- Location: LCCOMB_X32_Y16_N4
\U_LEDCTRL|LessThan4~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~3_cout\ = CARRY((\U_LEDCTRL|bpp_count\(1) & ((!\U_LEDCTRL|LessThan4~1_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a9\))) # (!\U_LEDCTRL|bpp_count\(1) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a9\ & 
-- !\U_LEDCTRL|LessThan4~1_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(1),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a9\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan4~1_cout\,
	cout => \U_LEDCTRL|LessThan4~3_cout\);

-- Location: LCCOMB_X32_Y16_N6
\U_LEDCTRL|LessThan4~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~5_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a10\ & ((!\U_LEDCTRL|LessThan4~3_cout\) # (!\U_LEDCTRL|bpp_count\(2)))) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a10\ & (!\U_LEDCTRL|bpp_count\(2) & 
-- !\U_LEDCTRL|LessThan4~3_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a10\,
	datab => \U_LEDCTRL|bpp_count\(2),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan4~3_cout\,
	cout => \U_LEDCTRL|LessThan4~5_cout\);

-- Location: LCCOMB_X32_Y16_N8
\U_LEDCTRL|LessThan4~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~7_cout\ = CARRY((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\ & (\U_LEDCTRL|bpp_count\(3) & !\U_LEDCTRL|LessThan4~5_cout\)) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\ & 
-- ((\U_LEDCTRL|bpp_count\(3)) # (!\U_LEDCTRL|LessThan4~5_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a11~portbdataout\,
	datab => \U_LEDCTRL|bpp_count\(3),
	datad => VCC,
	cin => \U_LEDCTRL|LessThan4~5_cout\,
	cout => \U_LEDCTRL|LessThan4~7_cout\);

-- Location: LCCOMB_X32_Y16_N10
\U_LEDCTRL|LessThan4~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~9_cout\ = CARRY((\U_LEDCTRL|bpp_count\(4) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a12\ & !\U_LEDCTRL|LessThan4~7_cout\)) # (!\U_LEDCTRL|bpp_count\(4) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a12\) # 
-- (!\U_LEDCTRL|LessThan4~7_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(4),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a12\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan4~7_cout\,
	cout => \U_LEDCTRL|LessThan4~9_cout\);

-- Location: LCCOMB_X32_Y16_N12
\U_LEDCTRL|LessThan4~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~11_cout\ = CARRY((\U_LEDCTRL|bpp_count\(5) & ((!\U_LEDCTRL|LessThan4~9_cout\) # (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a13\))) # (!\U_LEDCTRL|bpp_count\(5) & (!\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a13\ & 
-- !\U_LEDCTRL|LessThan4~9_cout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(5),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a13\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan4~9_cout\,
	cout => \U_LEDCTRL|LessThan4~11_cout\);

-- Location: LCCOMB_X32_Y16_N14
\U_LEDCTRL|LessThan4~13\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~13_cout\ = CARRY((\U_LEDCTRL|bpp_count\(6) & (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a14\ & !\U_LEDCTRL|LessThan4~11_cout\)) # (!\U_LEDCTRL|bpp_count\(6) & ((\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a14\) # 
-- (!\U_LEDCTRL|LessThan4~11_cout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \U_LEDCTRL|bpp_count\(6),
	datab => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a14\,
	datad => VCC,
	cin => \U_LEDCTRL|LessThan4~11_cout\,
	cout => \U_LEDCTRL|LessThan4~13_cout\);

-- Location: LCCOMB_X32_Y16_N16
\U_LEDCTRL|LessThan4~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|LessThan4~14_combout\ = (\U_LEDCTRL|bpp_count\(7) & (\U_LEDCTRL|LessThan4~13_cout\ & \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a15\)) # (!\U_LEDCTRL|bpp_count\(7) & ((\U_LEDCTRL|LessThan4~13_cout\) # 
-- (\U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a15\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111001100110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|bpp_count\(7),
	datad => \U_MEMORY|ram_block_rtl_0|auto_generated|ram_block1a15\,
	cin => \U_LEDCTRL|LessThan4~13_cout\,
	combout => \U_LEDCTRL|LessThan4~14_combout\);

-- Location: FF_X32_Y16_N17
\U_LEDCTRL|s_rgb2[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \U_LEDCTRL|U_CLKDIV|clk_out~clkctrl_outclk\,
	d => \U_LEDCTRL|LessThan4~14_combout\,
	clrn => \ALT_INV_rst~clkctrl_outclk\,
	sclr => \U_LEDCTRL|ALT_INV_state.READ_PIXEL_DATA~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \U_LEDCTRL|s_rgb2\(1));

-- Location: LCCOMB_X34_Y13_N0
\U_LEDCTRL|s_oe~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \U_LEDCTRL|s_oe~0_combout\ = (\U_LEDCTRL|state.READ_PIXEL_DATA~q\) # (\U_LEDCTRL|state.INCR_RAM_ADDR~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \U_LEDCTRL|state.READ_PIXEL_DATA~q\,
	datad => \U_LEDCTRL|state.INCR_RAM_ADDR~q\,
	combout => \U_LEDCTRL|s_oe~0_combout\);

ww_clk_out <= \clk_out~output_o\;

ww_r1 <= \r1~output_o\;

ww_r2 <= \r2~output_o\;

ww_b1 <= \b1~output_o\;

ww_b2 <= \b2~output_o\;

ww_g1 <= \g1~output_o\;

ww_g2 <= \g2~output_o\;

ww_a <= \a~output_o\;

ww_b <= \b~output_o\;

ww_c <= \c~output_o\;

ww_d <= \d~output_o\;

ww_lat <= \lat~output_o\;

ww_oe <= \oe~output_o\;
END structure;


